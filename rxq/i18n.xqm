(: For conditions of distribution and use, see the accompanying legal.txt file. :)

module namespace max.i18n = 'pddn/max/i18n';
import module namespace max.util = 'pddn/max/util' at 'util.xqm';
declare variable $max.i18n:DEFAULT_FILE := "../ui/i18n/I18n.xml";

declare function max.i18n:getText($projectId as xs:string, $key as xs:string){
  max.i18n:getText($projectId, $key, ())
};

declare function max.i18n:getText($projectId as xs:string, $key as xs:string, $locale as xs:string ?)
{
  let $i18nResourceFile := if($locale)
                           then max.util:maxHome() ||"/ui/i18n/I18n" || $locale || ".xml"
                           else $max.i18n:DEFAULT_FILE  
  let $projectI18nFile :=   if($locale)
                           then max.util:maxHome() ||"/editions/" || $projectId || "/ui/i18n/I18n" || $locale || ".xml"
                           else max.util:maxHome() ||"/editions/" || $projectId || "/ui/i18n/I18n.xml"
  
  return
    try{
       if(doc($projectI18nFile)//entry[@key=$key and @type="html"])
       then doc($projectI18nFile)//entry[@key=$key]/*
       else if(doc($projectI18nFile)//entry[@key=$key])
       then doc($projectI18nFile)//entry[@key=$key]/text()
       else if(doc($i18nResourceFile)//entry[@key=$key])
            then doc($i18nResourceFile)//entry[@key=$key]/text()
            else $key
    } 
    catch * {                                                                                                   
      if(doc($i18nResourceFile)//entry[@key=$key])
      then doc($i18nResourceFile)//entry[@key=$key]/text()
      else $key
  }

};