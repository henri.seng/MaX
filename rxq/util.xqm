(: For conditions of distribution and use, see the accompanying legal.txt file. :)

module namespace max.util = 'pddn/max/util';
declare namespace xsl = "http://www.w3.org/1999/XSL/Transform";
import module namespace max.config = 'pddn/max/config' at 'config.xqm';
import module namespace max.cons = 'pddn/max/cons' at 'cons.xqm';
declare namespace tei="http://www.tei-c.org/ns/1.0";

(:
: Returns binary-file
:)
declare function max.util:rawFile($path) as item()+{
    web:response-header(map { 'media-type': web:content-type($path) }),
    file:read-binary($path)
      
};

declare function max.util:query($query, $bindings){
  xquery:eval($query, $bindings)
};

declare function max.util:dbNameFromCollection($collection){
  tokenize($collection,'/')[1]
};

(:XQueriyng + XSLT transformation:)
declare function max.util:queryAndTransform($query, $bindings, $xsltDoc as node() ?, $xsltParams as map(*)){
  let $xml := <div>{max.util:query($query, $bindings)}</div>
  return 
    if($xsltDoc)
    then xslt:transform($xml, $xsltDoc, $xsltParams)
    else $xml
};

(:adds xsl addons import to a main xsl one:)
declare function max.util:buildXSLTDoc($xsltMain as xs:string, $xsltAddons as xs:string *){

  copy $mainXSL := doc($xsltMain)
  modify(
    for $addon in $xsltAddons
       return insert node <xsl:import href="{$addon}"/> as last into $mainXSL/xsl:stylesheet
    
  ) 
  return $mainXSL  
};


(: XML DB Resources LISTING functions:)
(:
lists collection' sub collections 
:)
declare function max.util:children-collection($dbName, $collection){
  let $dbAndColl:= if(contains($dbName,'/'))
    then (substring-before($dbName,'/')[1],substring-after($dbName,'/'))
    else ($dbName,$collection)
  
  
  let $path := if($collection = '') then '' else $dbAndColl[2] || "/"
  return distinct-values(
    for $doc in db:list($dbAndColl[1]) where starts-with($doc, $path)
    let $subCollection := substring-before(substring-after($doc, $path), "/")
    return  
     if(normalize-space($subCollection) = '')
     then ()
     else $subCollection 
  )
};

(:
lists collection' sub collections and resources (recursively)
:)
declare function max.util:recursive-children-collection($dbName, $collection, $hrefPrefix){
  
    if($collection = "")
    then error("Parent collection can't be empty.")
    else
    
    (:documents:)
    let $docsOut := max.util:list-doc-in-collection($dbName,$collection,$hrefPrefix)
    
    (:subcollections:)  
    let $colls := 
      for $c in max.util:children-collection($dbName, $collection)
      let $childPath :=  $collection || "/" || $c
      return <li>{$c}{max.util:recursive-children-collection($dbName, $childPath, $hrefPrefix)}</li>
    
    (:result: docs + sub collections:)
    return <ul>{$docsOut,$colls}</ul>

};

(:
Lists collection 's document (not recursive)
:)
declare function max.util:doc-in-collection($dbName, $collection){
   let $dbAndColl:= if(contains($dbName,'/'))
    then (substring-before($dbName,'/')[1],substring-after($dbName,'/'))
    else ($dbName,$collection)
    
  for $doc in db:list($dbAndColl[1], $dbAndColl[2])
  let $path := if($dbAndColl[2]='') then '' else $dbAndColl[2] || '/'
  return if(($path || file:name($doc)) = $doc)
  then $doc
  else () 
  
};

(:
collection 's document HTML list(not recursive)
:)
declare function max.util:list-doc-in-collection($dbName, $collection, $hrefPrefix){
   let $docs := max.util:doc-in-collection($dbName,$collection)
   let $items := 
      if(count($docs) > 0)
      then
          for $d in $docs
          return
          try
          {
            let $root := local-name((doc($dbName || '/' || $d)//*)[1])
            let $title := 
                switch ($root)
                 case 'ead' return max.util:eadDocTitle($dbName, $d)
                  case 'TEI' return max.util:teiDocTitle($dbName, $d)
                  default return  tokenize($d,"/")[last()]
                  
            return             
             <li><a href="{$hrefPrefix || $d}">{$title}</a></li>
          }
          catch *{ <li class='error'>'Erreur: ' {$err:description}</li>}
      else ()
      
      (:ascending order:)
      for $item in $items order by $item//text() ascending return $item 
  
};


(:
Lists all DB resources (recursively)
:)
declare function max.util:list-db-resources($project){
    let $dbName := max.config:getProjectDBPath($project)
    let $xmlns:= max.config:getXMLFormat($project)
    let $baseURI:= max.config:getBaseURI()
    return
    <ul>
        {max.util:list-db-resources($project, $dbName, $baseURI, $xmlns,'')}
    </ul>

};



(:
Lists all DB resources (recursively) + add href prefix on each hyperlinks
:)

declare function max.util:list-db-resources($projectId, $dbName, $baseURI, $xmlns, $dir){
    for $d in db:dir($dbName, $dir)
    let $subDirs := max.util:list-db-resources($projectId, $dbName, $baseURI, $xmlns,$dir||'/'||$d)
    let $depth := count(string-to-codepoints($dir)[.=string-to-codepoints('/')])
    return
        let $fullPath := substring-after($dir || '/' || $d/text(),'/')
        return
            if(ends-with($d/text(),'.xml'))
            then
                <li data-depth='{$depth}' data-href='{$baseURI}{$projectId}/sommaire/{$fullPath}'>
                    {
                        max.util:getDocTitle($dbName, $fullPath, $xmlns)
                    }
                </li>
            else
                <li data-dir='true' data-depth='{$depth}'>{$d/text()}
                    <ul>
                        {max.util:list-db-resources($projectId, $dbName, $baseURI, $xmlns,$dir||'/'||$d)}
                    </ul>
                </li>
};




declare function max.util:getDocTitle($dbName, $docPath, $xmlns){
     switch ($xmlns)
        case $max.cons:EAD return max.util:eadDocTitle($dbName, $docPath)
        case $max.cons:TEI return max.util:teiDocTitle($dbName, $docPath)
        default return 'No title found  - ' || $xmlns
};

declare function max.util:teiDocTitle($dbName, $docPath){
    if(doc($dbName || '/' || $docPath)/tei:teiCorpus)
    then
        doc($dbName || '/' || $docPath)/tei:teiCorpus/tei:teiHeader/tei:fileDesc/tei:titleStmt
    else
        doc($dbName || '/' || $docPath)/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt
};

declare function max.util:eadDocTitle($dbName, $docPath){
     doc($dbName || '/' || $docPath)/*:ead/*:archdesc/*:did/*:unittitle//text()
};

declare function max.util:maxHome(){
  file:resolve-path(file:parent(file:parent(static-base-uri())))
};

(:
    Returns resource file path from the edition folder if exists, from the max default one if not.
:)
declare function max.util:getResourceFilePath($projectId, $path){
    let $editionFilePath := max.util:maxHome()  || "editions/" || $projectId || "/" || $path
    return
        if(file:exists($editionFilePath))
        then $editionFilePath
        else max.util:maxHome() || $path
};


(:---------------------------------------------------------------------------------:)

(:debug / help:)
declare function max.util:listContextFunction(){
  let $context := inspect:context()
  return 
  <ul>{
    for $f in $context//function
      return <li>NAME = {string($f/@name)} / URI = {string($f/@uri)}</li>
    }
  </ul>
  
};

