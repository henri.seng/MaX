(: For conditions of distribution and use, see the accompanying legal.txt file. :)

module namespace max.ead_index_treeview = 'pddn/max/ead_index_treeview';

declare function max.ead_index_treeview:subLevels($doc, $parent){
  let $children := for $ca in $doc//*:controlaccess where $ca/subject[text()=$parent]
                   return $ca/*:controlaccess
  return if (count($children) = 0)
    then ()
    else <list>{for $child in distinct-values($children/*:subject) order by $child
      return if(normalize-space($child)="")
        then()
        else<item>{$child}{max.ead_index_treeview:subLevels($doc,$child)}</item>}</list>             
  
};

declare function max.ead_index_treeview:countChildren($doc, $parent){
  let $children := for $ca in $doc//*:controlaccess where $ca/subject[text()=$parent]
                   return $ca/*:controlaccess
                   
  return count(for $child in distinct-values($children/*:subject)
      return if(normalize-space($child)="")
        then()
        else<item/>)                
};

declare function max.ead_index_treeview:allLevels($doc){
let $topLevels := for $i in distinct-values($doc//*:controlaccess[not(ancestor::*:controlaccess)]/descendant::*:subject[1]) order by $i
return
  let $trimed := normalize-space($i)
  return 
    if ($doc//*:controlaccess/*:controlaccess//*:controlaccess/*:subject[text() contains text {$trimed}]) 
    then () 
    else $trimed
    
return     
<root>{
for $top in $topLevels
  return<list>
      <item>{$top}</item>
      {max.ead_index_treeview:subLevels($doc, $top)}</list>}</root>
};


declare function max.ead_index_treeview:topLevels($doc){
if(count($doc//*:controlaccess//*:subject) = 0) 
then max.ead_index_treeview:flatIndex($doc)
else
let $topLevels := for $i in distinct-values($doc//*:controlaccess[not(ancestor::*:controlaccess)]/descendant::*:subject[1]) order by $i
return
  let $trimed := normalize-space($i)
  return 
    if ($doc//*:controlaccess/*:controlaccess//*:controlaccess/*:subject[text() contains text {$trimed}]) 
    then () 
    else $trimed
return     
  <root>{
  for $top in $topLevels
    return<list>
        <item nbchildren="{max.ead_index_treeview:countChildren($doc,$top)}">{$top}</item>
        </list>}</root>

};

declare function max.ead_index_treeview:flatIndex($doc){
  let $types := distinct-values($doc//*:controlaccess/*:genreform/@*:type)
  return
  <ul>{
    for $type in $types
    return 
        <li class="jstree-closed">{$type}
          <ul>
            {
              let $entries := $doc//*:controlaccess/*:genreform[@*:type=$type]/text()
              for $entry in distinct-values($entries)
              return <li><span class='indexEntry'>{$entry}</span><span class='badge'>{count(for $a in $entries where $a = $entry return $a)}</span></li>
            }
          </ul>
        </li>
  }
  </ul>
};
