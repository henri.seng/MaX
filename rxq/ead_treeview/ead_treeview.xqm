(: For conditions of distribution and use, see the accompanying legal.txt file. :)

module namespace ead_treeview = 'pddn/max/ead_treeview';

import module namespace max.ead_index_treeview = 'pddn/max/ead_index_treeview' at 'ead_index_treeview.xqm';
import module namespace max.config = 'pddn/max/config' at '../config.xqm';
import module namespace max.util = 'pddn/max/util' at '../util.xqm';

declare
%rest:GET
%output:method("html")
%rest:query-param("id","{$id}")
%rest:path("/editions/{$projectId}/treeview")
function ead_treeview:getView($projectId, $id){
  let $dbPath := max.config:getProjectDBPath($projectId)
  return if($id='#')
  then  max.util:list-db-resources($projectId)
  else 
    let $rewrittenID := replace($id, "%2F","/")   
    return
    if(ends-with($id, '.xml'))
    then 
    <ul>
      {
        for $c in doc($dbPath||"/"||$rewrittenID)/*:ead/*:archdesc/*:dsc/*:c 
        return <li>
                {$c/@id}{$c/*:did/*:unittitle//text()}
                {ead_treeview:browseEADFragment(doc($dbPath||"/"||$rewrittenID), string($c/@id))}
              </li>
      }     
    </ul>
    else () (:todo:)

};

declare
%rest:GET
%output:method("html")
%rest:query-param("level","{$level}")
%rest:query-param("doc","{$doc}")
%rest:path("editions/{$projectId}/treeview/index")
function ead_treeview:getIndexView($projectId, $level, $doc){
  let $dbPath := max.config:getProjectDBPath($projectId)
  return max.ead_index_treeview:topLevels(doc($dbPath || '/' ||$doc))
};


(:Access to jstree resources files (themes, js, ...):)
declare
%rest:GET
%rest:path("editions/plugins/treeview/resources/{$file=.+}")
function ead_treeview:getThemeResource($file){
  let $path := file:parent(static-base-uri()) || "/resources/"  || $file
  return max.util:rawFile($path)
};


declare function ead_treeview:browseEADFragment($doc, $id){
   if($doc/*:ead//*:c[@id=$id]/*:c)
   then 
    <ul>
    {
      for $c in $doc/*:ead//*:c[@id=$id]/*:c 
      let $nbChild := count($c/descendant::*:c)
      return 
      <li n="{$nbChild}" id="{file:name(base-uri($doc))||'%2F'||string($c/@id)}">
        <a href="{$c/@id}">{$c/*:did/*:unittitle//text()}</a>
      </li>
    }
  </ul>
  else()
};
