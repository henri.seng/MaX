(: For conditions of distribution and use, see the accompanying legal.txt file. :)

module namespace max.toc = 'pddn/max/toc';
import module namespace max.cons = 'pddn/max/cons' at 'cons.xqm';
import module namespace max.config = 'pddn/max/config' at 'config.xqm';
import module namespace max.html = 'pddn/max/html' at 'html.xqm';
import module namespace max.util = 'pddn/max/util' at 'util.xqm';
import module namespace max.tei_toc = "pddn/max/tei/tei_toc.xqm"  at 'tei/tei_toc.xqm';

(:get project table of contents:)
declare function max.toc:getProjectTOC($project){
    let $tocFile := file:parent(file:parent(static-base-uri()))
    ||'editions/'
    ||$project
    ||'/'
    ||$max.cons:TOC_QUERY_FILEPATH


    let $xml :=
        if(file:exists($tocFile))
        then
            xquery:invoke($tocFile,map
            {
            'project':$project,
            'baseURI': max.config:getBaseURI(),
            'dbPath':max.config:getProjectDBPath($project)
            })
        else
            max.util:list-db-resources($project)


    let $tocXSL := max.config:getProjectTOCXSL($project)
    return xslt:transform($xml,
            $tocXSL,
            map{
            "baseuri":max.config:getBaseURI(),
            "project": $project
            })/*[1]
};

(:
  Returns document's table of content
:)
 declare function max.toc:getDocumentTOC($project, $doc){
  
   let $f:= file:parent(file:parent(static-base-uri()))
                       ||'editions/'
                       ||$project
                       ||'/'
                       ||$max.cons:DOCUMENT_TOC_QUERY_FILEPATH 
                   
   let $xml :=
       if(file:exists($f))
           then
           xquery:invoke($f,map
                  { 'project':$project,
                    'baseURI': max.config:getBaseURI(),
                    'dbPath':max.config:getProjectDBPath($project),
                    'doc': $doc
                  })  
           else
           max.toc:buildDefaultDocumentTOC($project, $doc)

   let $tocXSL := max.config:getProjectDocumentTOCXSL($project)

   return xslt:transform($xml,
           $tocXSL,
           map{
           "baseuri":max.config:getBaseURI(),
           "project": $project,
           "docTitle":max.util:getDocTitle(max.config:getProjectDBPath($project),$doc, max.config:getXMLFormat($project))//*:title[1]
           })/*[1]
}; 

declare function max.toc:buildDefaultDocumentTOC($project, $docPath){
    let $xmlType := max.config:getXMLFormat($project)
    return switch($xmlType)
        case $max.cons:TEI
            return max.tei_toc:buildTEIDocumentTOC($project, $docPath)
        case $max.cons:EAD
            return <div>TODO EAD TOC</div>
        default return <div class='error'>Format XML {$xmlType} non géré !</div>

};

