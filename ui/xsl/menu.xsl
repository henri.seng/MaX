<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:param name="baseURI"/>
    <xsl:param name="selectedTarget"/>
    <xsl:param name="projectId"/>

    <xsl:template match="/">
        <xsl:variable name="topEntry"
                      select="/menu//entry[target/text()=$selectedTarget]/ancestor-or-self::entry[@type='main']"/>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <xsl:for-each select="/menu/entry[@type='main']">
                        <xsl:choose>
                            <xsl:when test="count(./entry) > 0">
                                <li class="nav-item dropdown">
                                    <xsl:if test="./id/text()=$topEntry/id/text()">
                                        <xsl:attribute name="class">active</xsl:attribute>
                                    </xsl:if>
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <xsl:choose>
                                            <xsl:when test="./label/child::*">
                                                <xsl:copy-of select="./label/child::*"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="./label/text()"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <xsl:for-each select="./entry">
                                            <a class="dropdown-item" href="{$baseURI}{$projectId}/{./target/text()}">
                                                <xsl:choose>
                                                    <xsl:when test="./label/child::*">
                                                        <xsl:copy-of select="./label/child::*"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="./label/text()"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </a>
                                        </xsl:for-each>
                                    </div>
                                </li>
                            </xsl:when>
                            <xsl:otherwise>
                                <li class='nav-item'>
                                    <xsl:if test="./id/text()=$topEntry/id/text()">
                                        <xsl:attribute name="class">active</xsl:attribute>
                                    </xsl:if>
                                    <a class="nav-link" href="{$baseURI}{$projectId}/{./target/text()}">
                                        <xsl:choose>
                                            <xsl:when test="./label/child::*">
                                                <xsl:copy-of select="./label/child::*"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="./label/text()"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </a>
                                </li>
                            </xsl:otherwise>
                        </xsl:choose>

                    </xsl:for-each>
                </ul>
            </div>
        </nav>

    </xsl:template>


</xsl:stylesheet>