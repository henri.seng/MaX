<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="tei">

    <xsl:param name="baseuri"/>
    <xsl:param name="project"/>
    <xsl:param name="docTitle"/>

    <xsl:template match="/">
        <div id="document-toc">
            <h1>Sommaire de
                <em>
                    <xsl:value-of select="$docTitle"/>
                </em>
            </h1>
            <xsl:apply-templates/>

        </div>
    </xsl:template>

    <xsl:template match="tei:ul">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

    <xsl:template match="tei:li">
        <li>
            <xsl:attribute name="id">
                <xsl:value-of select="@id"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </li>
    </xsl:template>

    <xsl:template match="tei:head">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="../@data-href"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </a>
    </xsl:template>

    <xsl:template match="tei:hi">
        <span>
            <xsl:attribute name="class">
                <xsl:value-of select="@rend"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:author">
    </xsl:template>

    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>


</xsl:stylesheet>