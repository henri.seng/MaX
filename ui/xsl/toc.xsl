<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:import href="tei/document_title.xsl"/>
    
    <xsl:param name="baseuri"/>
    <xsl:param name="project"/>

    <xsl:template match="/">
        <div id="toc">
            <h1>Sommaire</h1>
            <xsl:apply-templates/>
        </div>

    </xsl:template>

    <xsl:template match="li">
        <li>
            <xsl:choose>
                <xsl:when test="@data-dir='true'">
                    <xsl:value-of select="./text()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:variable name="href">
                        <xsl:value-of select="@data-href"/>
                    </xsl:variable>
                    <a href="{$href}">
                        <xsl:apply-templates/>
                    </a>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="./ul"/>
        </li>
    </xsl:template>

    <xsl:template match="ul">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

</xsl:stylesheet>