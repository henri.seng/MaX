window.MAX_LOCAL_STORAGE_PREFIX = "MaXLS.";
window.MAX_VISIBLE_PROPERTY_STRING = "visible";
window.NOTE_TO_TXT_CLASS = ".note_to_text";

class Max {
    constructor(pid, baseURL, pluginsURL, fid) {
        this.projectID = pid;
        this.baseURL = baseURL;
        this.pluginsBaseURL = pluginsURL;
        this.plugins = [];
        this.fragmentID = fid;
    }

    run() {
        if (this.isInFragmentContext()) {
            if (this.isWithNotes()) {
                //ajout d'une colonne de notes + placement
                this.manageMarginNotes();
            }

            this.bindNavigationTool();
            this.bindTooltips();
            this.runPlugins();
            this.textOptions();

            //affiche le bas de page si non vide
            if ($("#bas_de_page .footnote").length > 0)
                $("#bas_de_page").show();
        }
    }

    getProjectID() {
        return this.projectID;
    }

    getFragmentID() {
        return this.fragmentID;
    }

    addPlugin(p) {
        this.plugins[p.getName()] = p;
    }

    getBaseURL() {
        return this.baseURL;
    }

    getPluginsBaseURL() {
        return this.pluginsBaseURL;
    }

    scrollToID(id) {
        this.scrollToElement($("#" + id.replace(".", "\\.")));
    }

    scrollToElement(element, delay) {
        let offset = $('#topbar').length === 1 ? $('#topbar').height()
            + ($('#breadcrumb').length === 1 ? $('#breadcrumb').height() : 0) + 10 : 0;
        if (element.offset() != null)
            $('html, body').animate({
                scrollTop: element.offset().top - offset
            }, delay);
    }

    textOptions() {
        //shows option's button if needed
        if ($("#options-list li").length > 0) {
            $("#txt_options").show();
        }

        //checkbox options (load from localstorage)
        $(".visibility_toggle").each(function () {
            let optionName = $(this).data('option');
            if (localStorage.getItem(window.MAX_LOCAL_STORAGE_PREFIX + optionName) === window.MAX_VISIBLE_PROPERTY_STRING) {
                $(this).attr("checked", "checked");
                //updates visibility
                $("." + optionName).show();
            } else {
                $(this).removeAttr("checked");
                //updates visibility
                $("." + optionName).hide();
            }

        });
    }

    manageMarginNotes() {
        let mainContainer = $("#main-max-container");
        let leftBloc = mainContainer.children(":first");
        leftBloc.addClass("row");
        $("#text").addClass("col-sm-8");
        leftBloc.append("<div id='margin_notes' class='col-sm-2'></div>");

        let previousNote = null;
        $(".manchette_droite").each(function () {
            //stocke le top original avant de déplacer la note
            let originalTop = $(this).position().top;
            //déplacement de la note dans la colonne prévue
            $(this).appendTo("#margin_notes");
            //mise à jour top (+anti-chevauchement)
            let newTop = originalTop;
            if (previousNote != null && previousNote.position() != null) {
                let bottomPrevious = previousNote.position().top + previousNote.height();
                if (bottomPrevious + 5 > newTop) {
                    newTop = bottomPrevious + 5;
                }
            }
            $(this).css("top", newTop + "px");
            previousNote = $(this);
        });

    }

    /*Branchements 'onClick' sur les flèches de la barre de navigation*/
    bindNavigationTool() {
        //local function : find next or previous nav link
        let nextOrPrevious = function (step) {
            let current = $("#navigation-tool").find('button').attr("id").replace("selected-", "");//remove 'selected' prefix
            let links = $('#dropdown-navigation').find('li');
            let position = links.index(document.getElementById(current));
            let nextOrPrevious = links.get(position + step);
            window.location.href = $(nextOrPrevious).find('a').attr("href");
        };

        if ($('#nav_previous').length) {
            $('#nav_previous').click(
                function () {
                    nextOrPrevious(-1);
                });
        }
        if ($('#nav_next').length) {
            $('#nav_next').click(
                function () {
                    nextOrPrevious(1);
                });
        }
    }

    bindTooltips() {
        try {
            $('[data-toggle="tooltip"]').each(function () {
                let noteNumber = $(this).attr('id').substr(5);
                //clones bottom page note
                let noteContent = $("#wrap_bdp_" + noteNumber).clone();
                //removes note call (</a>)
                noteContent.find(window.NOTE_TO_TXT_CLASS).remove();
                //tooltip mapping
                $(this).tooltip(
                    {
                        html: true, // allows html content
                        title: noteContent.html(),
                        delay: {
                            "show": 0,//instantatly shown
                            "hide": 3000 //displayed during 3seconds
                        }
                    }
                );
            });
        } catch (e) {
            console.log('tooltips error ' + e);
        }
    }

    runPlugins() {
        Object.keys(this.plugins).forEach((k) =>{
            this.plugins[k].run();
        })
    }

    /*
     Vérifie si le contexte courant (page) est une consultation
     d'un fragment (présence d'un élement identifié 'text')
   */
    isInFragmentContext() {
        return $("#text").length > 0;
    }

    isWithNotes() {
        return $(".appel_note_marge").length > 0;
    }

    /*Affichage / masquage des éléments d'une classe CSS depuis un checkBox
    des options d'affichage
    */
    setClassVisibility(className) {

        if ($('#toggle_' + className).prop("checked")) {
            $("." + className).show();
            localStorage.setItem(window.MAX_LOCAL_STORAGE_PREFIX + className, window.MAX_VISIBLE_PROPERTY_STRING);
        } else {
            $("." + className).hide();
            localStorage.removeItem(window.MAX_LOCAL_STORAGE_PREFIX + className);
        }
    }

    toString() {
        return this.projectID + "s MaX";
    }
}

window.MAX = new Max(projectId, baseURI, pluginsBaseURI, fragmentId);

$(document).ready(function () {
    "use strict";
    window.MAX.run();
});

