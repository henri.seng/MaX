import {Plugin} from '../../core/ui/js/Plugin.js';

const CORR_CLASS = "corr";
const SIC_CLASS = "sic";

class CorrectionPlugin extends Plugin{
  constructor(name) {
    super(name);
  }

  run(){
    console.log("plugin correction running")
    var checkAttr = "";
    if(localStorage.
           getItem(MAX_LOCAL_STORAGE_PREFIX + CORR_CLASS) === MAX_VISIBLE_PROPERTY_STRING) 
    {    
      checkAttr = "checked='checked' ";
      //updates visibility
      this.on();
    }
    else{  
      //updates visibility 
      this.off();
     }

    $("#options-list").append("<li><a><input id='toggle_corr' type='checkbox' "
          +checkAttr
          +" name='toggle_corr'>Afficher les corrections</a></li>");
    let self = this;      
    $('#toggle_corr').change(function(){
      self.setCorrVisible()
    })       
  }
  
  setCorrVisible(){
    if($('#toggle_corr').is(":checked"))
    {
      this.on();
      localStorage.setItem(MAX_LOCAL_STORAGE_PREFIX + CORR_CLASS, MAX_VISIBLE_PROPERTY_STRING);
    }
    else{ 
      this.off(); 
      localStorage.removeItem(MAX_LOCAL_STORAGE_PREFIX + CORR_CLASS);
    }  
  }
  
  
  /*corr visibles*/
  on(){
    $("." + SIC_CLASS).hide(); 
    $("." + CORR_CLASS).show(); 
  }
  /*corr hidden (sic visibles)*/
  off(){
    $("." + SIC_CLASS).show(); 
    $("." + CORR_CLASS).hide();
  }
}

MAX.addPlugin(new CorrectionPlugin('Correction'));
