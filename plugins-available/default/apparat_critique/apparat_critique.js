import {Plugin} from '../../core/ui/js/Plugin.js';
const PLUGIN_NAME='apparat';

class ApparatPlugin extends Plugin{
  constructor(name) {
    super(name);
  }

  run(){
      //si le fragment courant ne contient pas d'apparat
      if(!this.docWithApparat()){
         $(".apparat-witnesses").hide();
         return;
      }
      this.showWitness("lem");
      this.witnessTooltiping();
  }
  
  showWitness(witnessClass){
    $(".apparat").hide();
    $("."+witnessClass).show();
    this.hideLacunas(witnessClass);
  }

  
  witnessTooltiping(){
    $('[data-witnesses]').each(function(){
        var wlist = $(this).attr("data-witnesses").replace(" ",", "); 
        $(this).attr("title", wlist);  
        $(this).tooltip(
           {
            html:true, // allows html content
            content: "<div><em>Témoins:</em> "+wlist+"</div>"
           }
        );
    });

  }

  hideLacunas(witId){

  /*masque les lacunas à partir des lacunaStarts
    - cherche la lacuneEnd correspondante
    - Si la lacunaEnd n'existe pas (elle se trouve dans un autre fragment), 
     l'intégralité du texte suivant est masqué 
  */
  var self = this;
  $('.lacunaStart').each(function(){
    if($(this).attr('data-lacuna-wit').indexOf('\#'+witId)>-1){
      //on echappe les . et # de l'id cible de la lacunaEnd correspondante
      var searchedId = "#" + $(this).attr('data-lacuna-synch').replace(/([#.])/g,'\\$1')
      var endElement = $(searchedId)
      console.log('traitement de la lacuna '+ $(this).attr('id'));
      endElement = endElement.length === 0 ? $('#bas_de_page') : endElement
      var eltsBetween = $(self.getElementsBetweenTree(($(this))[0], endElement[0]))
      console.log("Nombre d'éléments à masquer (lacune "+$(this).attr('id')+")"+eltsBetween.length);
      eltsBetween.each(function(){
        $(this).wrap("<span class='generated_lacuna'></span>")
      })
    }
  })
  
  /*Cas où un couple lacunaStart/lacunaEnd se trouve respectivement dans
  des fragments qui précédent et suivent le fragment courant:
  nécessite une requête sur la DB pour accedéer aux autres noeuds XML*/
  
  }
  
  getElementsBetweenTree(start, end) {
    var ancestor= this.getCommonAncestor(start, end);

    var before= [];
    while (start.parentNode!==ancestor) {
        var el= start;
        while (el.nextSibling)
            before.push(el= el.nextSibling);
        start= start.parentNode;
    }

    var after= [];
    while (end.parentNode!==ancestor) {
        var el= end;
        while (el.previousSibling)
            after.push(el= el.previousSibling);
        end= end.parentNode;
    }
    after.reverse();

    while ((start= start.nextSibling)!==end)
        before.push(start);
    return before.concat(after);
}

  // Get the innermost element that is an ancestor of two nodes.
  //
  getCommonAncestor(a, b) {
      var parents= $(a).parents().addBack();
      while (b) {
          var ix= parents.index(b);
          if (ix!==-1)
              return b;
          b= b.parentNode;
      }
      return null;
  }


  docWithApparat(){
    return ($("#text .apparat").length + $("#text .lacunaStart").length + $("#text .lacunaEnd").length) > 0
  }

}

let apparat = new ApparatPlugin('Apparat')
window.apparat = apparat;
MAX.addPlugin(apparat);