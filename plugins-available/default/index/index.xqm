(: For conditions of distribution and use, see the accompanying legal.txt file. :)

module namespace max.plugin.index = 'pddn/max/plugin/index';
import module namespace max.config = 'pddn/max/config' at '../../rxq/config.xqm';
import module namespace max.html = 'pddn/max/html' at '../../rxq/html.xqm';

(:
returns index fragment if exists (already generated)
returns a news generated one if not
:)
declare
%rest:GET
%output:method("html")
%rest:path("/editions/{$project}/index")
function max.plugin.index:index($project){
  let $indexFilePath :=file:parent(file:parent(file:parent(static-base-uri()))) || '/editions/'||$project||'/fragments/index.frag.html'
  return
    if(file:exists($indexFilePath))
    then 
          let $content := <div id='content'>{doc($indexFilePath)}</div>
          return max.html:render($project, 'index', $content)
    else max.plugin.index:generateIndex($project)
};

declare function max.plugin.index:generateIndex($project){
  try{
    let $indexFilePath :=file:parent(file:parent(file:parent(static-base-uri()))) || '/editions/'||$project||'/fragments/index.frag.html'
    let $dbPath := max.config:getProjectDBPath($project)
    let $backToTextPath := max.config:getPluginParameterValue($project, 'index', 'backToText')
    let $persons := <listPerson>
    {
      for $i at $p in collection($dbPath)//*:name[@*:type='personne' and @*:ref]
        group by $ref:=$i/@*:ref
        return if(starts-with($ref,'http'))
        then ()
        else
        let $id := substring-after($ref,'#')
        return if(not($id))
                         then error(xs:QName('max.error'),'ID IS -' || $ref)
                else
                let $url := max.config:getPluginParameterValue($project, 'index', 'url') || $id ||'.xml'
                let $request :=
                <http:request href='{$url}' 
                  method='post' username="{max.config:getPluginParameterValue($project, 'index', 'login')}"
                  password="{max.config:getPluginParameterValue($project, 'index', 'password')}" send-authorization='true'>
                  <http:body media-type='application/xml'>
                    <query xmlns="http://basex.org/rest">
                      <text><![CDATA[
                          <person xml:id='{//*:TEI/@xml:id/string()}'>{//*:TEI//*:person}
                          </person>
                          ]]></text>
                    </query>
                  </http:body>
                </http:request>
               let $person := http:send-request($request)
               let $occurs:=
                       <div type='occurs'>{
                         for $n at $j in collection($dbPath)//*:name[@*:type='personne' and @*:ref=$ref]
                           let $target:=substring-after(base-uri($n),$dbPath)||'/'||xquery:eval($backToTextPath, map { '': $n})
                           return <a target="./{$target}">{$j}</a>
                         }
                       </div>
               return
                 <item>{$person}{$occurs}</item>

     }</listPerson>
     let $HTMLIndex := xslt:transform($persons, 'indexing.xsl', ())
     (:return file:write($indexFilePath, <div class='index'>{$HTMLIndex}</div>), max.plugin.index:index($project):)
     return  max.html:render($project, 'index', <div class='index'>{$HTMLIndex}</div>)
   }
   catch experr:HC0001{<div>Check your Index Plugin Config- TRACE:  {$err:description} -  {$err:module} - {$err:line-number}</div>}      
};


declare %private function max.plugin.index:generateIndexToDonwload($project){
  
  ()
};