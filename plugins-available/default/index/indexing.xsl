<?xml version="1.0" encoding="UTF-8"?>
<!--
 For conditions of distribution and use, see the accompanying legal.txt file.
-->

<xsl:stylesheet	version="2.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="tei xsl">
                
                <xsl:output method="xml" encoding="utf-8"/>
<xsl:template match='/'>  
  <ul>
    <xsl:for-each-group select="//tei:person" group-by="substring(normalize-space(string-join(.//tei:persName//text(),'')),1,1)">
      <xsl:sort select="current-grouping-key()" />
      <li class="index-letter-block">
        <h2><xsl:value-of select="current-grouping-key()"/></h2>
            <ul>
              <xsl:for-each select="current-group()">
                <xsl:sort select="." />          
                <li class='index-block'><h3><xsl:value-of select="./ancestor::*:person[last()]/@xml:id"/></h3>
                  <ul>
                    <xsl:apply-templates />
                  </ul>
                  <xsl:apply-templates select="./ancestor::*:item[last()]/*:div[@*:type='occurs']"/>
                </li>
             </xsl:for-each>
           </ul>
        </li>
    </xsl:for-each-group>
  </ul>
</xsl:template>
     
<xsl:template match='tei:persName'>
  <xsl:variable name='class'><xsl:value-of select="@xml:lang"/></xsl:variable>
  <!--<xsl:if test="$class='fr'">
    <li>ID = <xsl:value-of select="./ancestor::*:person[last()]/@xml:id"/></li>
  </xsl:if>-->
  <li class='{$class}'><xsl:value-of select="."/><span class='lang'> [<xsl:value-of select="$class"/>]</span></li>
</xsl:template>     
     
<xsl:template match='tei:birth'>
  <li class='birth'>Né(e) le <xsl:value-of select="."/></li>
</xsl:template> 
<xsl:template match='tei:death'>
  <li class='death'>Décédé(e) le <xsl:value-of select="."/></li>
</xsl:template>

<xsl:template match='div'>
  <div class='occurs'><xsl:apply-templates /></div>
</xsl:template>
<xsl:template match='a'>
  <a class='index-link' href='{./@target}'><xsl:value-of select="."/></a>
</xsl:template>

<xsl:template match='tei:occupation | tei:note'>
<!-- TODO -->
</xsl:template>


<!--  
           
<xsl:for-each-group select="record" group-by="info/date">
</xsl:for-each-group>
  -->
</xsl:stylesheet>