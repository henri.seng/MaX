import {Plugin} from '../../core/ui/js/Plugin.js';

class ImageViewer extends Plugin{
  constructor(name) {
    super(name);

    var viewables = document.querySelectorAll('#text .img_viewer_link'); //img + pb
    var images = document.querySelectorAll('#text img');//img only
    if(viewables.length > 0) {
      $('#js_scripts').append('<script type="text/javascript" src="' +baseURI + '/core/ui/lib/openseadragon/openseadragon.min.js"></script>');
      $('body').prepend('<div id="img_dialog_wrap" class="modal" tabindex="-1" role="dialog">'+
                '<div class="modal-dialog" role="document">'+
                  '<div class="modal-content">'+
                    '<div class="modal-header">'+
                      '<h5 class="modal-title" id="img-title"></h5>'+
                      '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
                        '<span aria-hidden="true">&times;</span>'+
                      '</button>'+
                    '</div>'+
                    '<div class="modal-body">'+
                      '<p>Modal body text goes here.</p>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>');
      $('#img_dialog_wrap').hide();
      $('.viewable').css('cursor', 'pointer');

         if(images.length > 1)
              $('#main-max-container').append('<div>'
              +'<button id="all-images-btn" class="btn btn-secondary" onclick="MAX.plugins[\'img_viewer\'].openImagesInDialog()">'
              +'<img alt="voir la galerie d\'images de la page" title="voir la galerie d\'images de la page" src="' + baseURI + 'plugins/img_viewer/images/gallery.png"/>'
              +'</button></div>')
    }
  }


    openImageInDialog(href){
        let tiled = href.endsWith('dzi') ||  href.endsWith('json') ;
        this.showImages(tiled ?  href : {type: 'image', url:  href })
    }

    showImages(tileSources){
       $('#img_dialog_wrap .modal-body').html('');
       $('#img_dialog_wrap .modal-body').append('<div id="osd-viewer" style="display: flex;"></div>')
       OpenSeadragon({
                    id:            "osd-viewer",
                    prefixUrl:     baseURI + "core/ui/lib/openseadragon/images/",
                    tileSources:   tileSources,
                    sequenceMode:  tileSources.length > 1
                });
      $('#img_dialog_wrap').modal();
    }

    openImagesInDialog(){
        var tileSources = [];
        var images = document.querySelectorAll('#text img');
        for(var i = 0; i < images.length; i++){
            tileSources.push({'type' : 'image', 'url' : images[i].src})
        }
        this.showImages(tileSources)
    }

}


MAX.addPlugin(new ImageViewer('img_viewer'));

