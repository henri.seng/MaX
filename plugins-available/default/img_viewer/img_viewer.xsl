<?xml version="1.0" encoding="UTF-8" ?>
<!--
 For conditions of distribution and use, see the accompanying legal.txt file.
-->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="tei xsl">

    <xsl:param name="imagesRepository"/>

    <xsl:template match="tei:pb">
        <xsl:variable name="href">
            <xsl:choose>
                <xsl:when test="starts-with(@facs,'http')">
                    <xsl:value-of select="@facs"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat($baseuri,$project,'/',$imagesRepository,@facs)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <a>
            <xsl:attribute name="class">pb img_viewer_link</xsl:attribute>
            <xsl:attribute name="name">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:attribute name="href">#</xsl:attribute>
            <xsl:attribute name="onclick">MAX.plugins['img_viewer'].openImageInDialog('<xsl:value-of select="$href"/>')</xsl:attribute>
            <xsl:value-of select="@n"/>
        </a>
    </xsl:template>


    <xsl:template match="tei:figure">
        <div class="figure">
            <xsl:apply-templates/>
        </div>
    </xsl:template>


    <xsl:template match="tei:graphic">
        <xsl:variable name="href">
            <xsl:value-of select="concat($baseuri,$project,'/',$imagesRepository ,@url)"/>
        </xsl:variable>
        <xsl:variable name="id">
            <xsl:value-of select="generate-id(.) "/>
        </xsl:variable>
        <img class="viewable img_viewer_link" id="{$id}">
            <xsl:attribute name="onclick">MAX.plugins['img_viewer'].openImageInDialog('<xsl:value-of select="$href"/>')</xsl:attribute>
            <xsl:attribute name="src">
                <xsl:value-of select="concat($baseuri,$project, '/ui/images/',@url)"/>
            </xsl:attribute>
        </img>
    </xsl:template>


</xsl:stylesheet>                