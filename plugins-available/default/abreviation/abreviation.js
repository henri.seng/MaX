
import {Plugin} from '../../core/ui/js/Plugin.js';

const EX_CLASS = "ex";
const AM_CLASS = "am";

class AbreviationPlugin extends Plugin{
  constructor(name) {
    super(name);
  }

  run(){
    console.log("Plugin abréviation running")
    var checkAttr = "";
    if(localStorage.
           getItem(MAX_LOCAL_STORAGE_PREFIX + EX_CLASS) === MAX_VISIBLE_PROPERTY_STRING)
    {
      checkAttr = "checked='checked' ";
      //updates visibility
      this.on();
    }
    else{
      //updates visibility
      this.off();
     }

    $("#options-list").append("<li><a><input id='toggle_ex' type='checkbox' "
          +checkAttr
          +" name='toggle_ex'>Développer les abréviations</a></li>");
    let self = this;
    $('#toggle_ex').change(function(){
      self.setExVisible()
    })
  }

  setExVisible(){
    if($('#toggle_ex').is(":checked"))
    {
      this.on();
      localStorage.setItem(MAX_LOCAL_STORAGE_PREFIX + EX_CLASS, MAX_VISIBLE_PROPERTY_STRING);
    }
    else{
      this.off();
      localStorage.removeItem(MAX_LOCAL_STORAGE_PREFIX + EX_CLASS);
    }
  }


  /*ex visibles*/
  on(){
    $("." + AM_CLASS).hide();
    $("." + EX_CLASS).show();
  }
  /*corr hidden (sic visibles)*/
  off(){
    $("." + AM_CLASS).show();
    $("." + EX_CLASS).hide();
  }
}

MAX.addPlugin(new AbreviationPlugin('Abreviation'));
