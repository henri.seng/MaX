import {Plugin} from '../../core/ui/js/Plugin.js';

const CORR_CLASS = "corr";
const SIC_CLASS = "sic";
const ORIG_CLASS = "orig";
const REG_CLASS = "reg";
const EX_CLASS = "ex";
const AM_CLASS = "am";

class DiplomatiquePlugin extends Plugin{
  constructor(name) {
    super(name);
  }

  run(){
    console.log("plugin diplomatque running")
    var checkAttr = "";
    if(localStorage.
           getItem(MAX_LOCAL_STORAGE_PREFIX + CORR_CLASS) === MAX_VISIBLE_PROPERTY_STRING &&  localStorage.
           getItem(MAX_LOCAL_STORAGE_PREFIX + REG_CLASS) === MAX_VISIBLE_PROPERTY_STRING && localStorage.
           getItem(MAX_LOCAL_STORAGE_PREFIX + EX_CLASS) === MAX_VISIBLE_PROPERTY_STRING)
    {    
      checkAttr = "checked='checked' ";
      //updates visibility
      this.on();
    }
    else{  
      //updates visibility 
      this.off();
     }

    $("#options-list").append("<li><a><input id='toggle_diplo' type='checkbox' "
          +checkAttr
          +" name='toggle_diplo'>Affichage régularisé</a></li>");
    let self = this;      
    $('#toggle_diplo').change(function(){
      self.setCorrVisible()
    })       
  }
  
  setCorrVisible(){
    if($('#toggle_diplo').is(":checked"))
    {
      this.on();
      localStorage.setItem(MAX_LOCAL_STORAGE_PREFIX + CORR_CLASS, MAX_VISIBLE_PROPERTY_STRING);
      localStorage.setItem(MAX_LOCAL_STORAGE_PREFIX + REG_CLASS, MAX_VISIBLE_PROPERTY_STRING);
      localStorage.setItem(MAX_LOCAL_STORAGE_PREFIX + EX_CLASS, MAX_VISIBLE_PROPERTY_STRING);
    }

    else{ 
      this.off(); 
      localStorage.removeItem(MAX_LOCAL_STORAGE_PREFIX + CORR_CLASS);
      localStorage.removeItem(MAX_LOCAL_STORAGE_PREFIX + REG_CLASS);
      localStorage.removeItem(MAX_LOCAL_STORAGE_PREFIX + EX_CLASS);
    }    
  }
  
  
  /*corr visibles*/
  on(){
    $("." + SIC_CLASS).hide(); 
    $("." + CORR_CLASS).show();
    $("." + ORIG_CLASS).hide(); 
    $("." + REG_CLASS).show();
    $("." + AM_CLASS).hide(); 
    $("." + EX_CLASS).show(); 
  }
  /*corr hidden (sic visibles)*/
  off(){
    $("." + SIC_CLASS).show(); 
    $("." + CORR_CLASS).hide();
    $("." + ORIG_CLASS).show(); 
    $("." + REG_CLASS).hide();
    $("." + AM_CLASS).show(); 
    $("." + EX_CLASS).hide();
  }
}

MAX.addPlugin(new DiplomatiquePlugin('Diplomatique'));
