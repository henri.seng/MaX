const REG_CLASS = "reg";
const ORIG_CLASS = "orig";

var normalisationOptions = new Object();
  
  
normalisationOptions['onTextChange'] = function() { 
  
  var checkAttr = "";
  if(localStorage.
         getItem(MAX_LOCAL_STORAGE_PREFIX + REG_CLASS) === MAX_VISIBLE_PROPERTY_STRING) 
  {    
    checkAttr = "checked='checked' ";
    //updates visibility
    onNorm();
  }
  else{  
    //updates visibility 
    offNorm();
   }

 $("#options-list").append("<li><a><input id='toggle_reg' type='checkbox' onchange='setRegVisible()' "+checkAttr+" name='toggle_reg'>Normalisation de l'éditeur</a></li>");
}


function setRegVisible(){

  if($('#toggle_reg').is(":checked"))
  {
    onNorm();
    localStorage.setItem(MAX_LOCAL_STORAGE_PREFIX + REG_CLASS, MAX_VISIBLE_PROPERTY_STRING);
  }
  else{ 
    offNorm(); 
    localStorage.removeItem(MAX_LOCAL_STORAGE_PREFIX + REG_CLASS);
  }  
}

/*corr visibles*/
function onNorm(){
  $("." + ORIG_CLASS).hide(); 
  $("." + REG_CLASS).show(); 
}
/*corr hidden (sic visibles)*/
function offNorm(){
  $("." + ORIG_CLASS).show(); 
  $("." + REG_CLASS).hide();
}

MAX.addModule(normalisationOptions);