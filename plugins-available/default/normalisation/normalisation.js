import {Plugin} from '../../core/ui/js/Plugin.js';

const REG_CLASS = "reg";
const ORIG_CLASS = "orig";

class NormalisationPlugin extends Plugin{
  constructor(name) {
    super(name);
  }

  run(){
    console.log("plugin normalisation running")
    var checkAttr = "";
    if(localStorage.
           getItem(MAX_LOCAL_STORAGE_PREFIX + REG_CLASS) === MAX_VISIBLE_PROPERTY_STRING)
    {
      checkAttr = "checked='checked' ";
      //updates visibility
      this.on();
    }
    else{
      //updates visibility
      this.off();
     }

    $("#options-list").append("<li><a><input id='toggle_reg' type='checkbox' "
          +checkAttr
          +" name='toggle_reg'>Normalisation de l’éditeur</a></li>");
    let self = this;
    $('#toggle_reg').change(function(){
      self.setRegVisible()
    })
  }

  setRegVisible(){
    if($('#toggle_reg').is(":checked"))
    {
      this.on();
      localStorage.setItem(MAX_LOCAL_STORAGE_PREFIX + REG_CLASS, MAX_VISIBLE_PROPERTY_STRING);
    }
    else{
      this.off();
      localStorage.removeItem(MAX_LOCAL_STORAGE_PREFIX + REG_CLASS);
    }
  }


  /*reg visibles*/
  on(){
    $("." + ORIG_CLASS).hide();
    $("." + REG_CLASS).show();
  }
  /*reg hidden (orig visibles)*/
  off(){
    $("." + ORIG_CLASS).show();
    $("." + REG_CLASS).hide();
  }
}

MAX.addPlugin(new NormalisationPlugin('Normalisation'));