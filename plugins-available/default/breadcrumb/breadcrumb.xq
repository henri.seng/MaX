xquery version "3.0";
declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace max.config = 'pddn/max/config' at '../../rxq/config.xqm';
import module namespace max.util = 'pddn/max/util' at '../../rxq/util.xqm';

declare variable $baseURI external;
declare variable $dbPath external;
declare variable $project external;
declare variable $doc external;
declare variable $id external;

declare variable $PLUGIN_ID := "breadcrumb";
declare variable $TOP_LABEL := "topLabel";

let $projectPrettyName := max.config:getPluginParameterValue($project, $PLUGIN_ID, $TOP_LABEL)
let $title:= max.util:getDocTitle($dbPath, $doc, max.config:getXMLFormat($project))
let $xsl := max.config:getDocumentTitleTOCXSL($project)
let $transformedTitle := xslt:transform($title,$xsl)

return
    <div id="breadcrumb">
        <ol class="breadcrumb">
            <li><a href = "{$baseURI || $project}/sommaire">{$projectPrettyName}</a></li>
            <li><a href = "{$baseURI  ||$project}/sommaire/{$doc}">{$transformedTitle}</a></li>
            {
                if($id)then <li>{
                    xslt:transform(
                            collection($dbPath)//*[@xml:id = $id],
                            file:parent(static-base-uri()) || '_breadcrumb.xsl')
                }
                </li>
                else()
            }
        </ol>
    </div>
