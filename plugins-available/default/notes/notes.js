$(document).ready(function(){
	tooltipNotes(); 
  
})

function tooltipNotes() {
	$(".note-appel").each(function() {
	var contentTooltip = $(this).next(".texteNote").html();
	$(this).popover(
		{ 
		  html    : true, 
		  content : contentTooltip, 
		  placement:"auto",
		  animation:true,
        interactive:true,
		 /* title   : '<p>Un titre</p>' */
		});
	});
	
	 $("a.note-appel").mouseover(function()
    {  
        $(this).popover("show");// pour afficher une infobulle complexe
    }); 
    
  /*  $('a.note-appel').on('shown.bs.popover', function() {
    setTimeout(function() {
        $('a.note-appel').popover('hide');
    }, 2000);
	});*/
    	 
   $("a.note-appel").mouseout(function(){  
        setTimeout(function() {
        $('a.note-appel').popover('hide');
    	}, 2000);
    } ); 
	
}