<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright Université de Caen Normandie
Esplanade de la paix
CS 14032
14032 Caen CEDEX 5
Contributeur : Jérôme Chauveau,
2016

jerome.chauveau@unicaen.fr

Ce logiciel est un programme informatique servant à [rappeler les
caractéristiques techniques de votre logiciel]. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.
=========================================================================
Copyright Université de Caen Normandie
Esplanade de la paix
CS 14032
14032 Caen CEDEX 5
Contributor : Jérôme Chauveau,
2016

jerome.chauveau@unicaen.fr

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
-->

<xsl:stylesheet	version="2.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="tei xsl">


  	<!--LES NOTES DE scribe -->    
	<xsl:template match="tei:note[@place='margin-right']" >
		<p>
	  	<xsl:attribute name="class">manchette_droite ui-corner-all</xsl:attribute>
	    <xsl:attribute name="id">note_droite</xsl:attribute>		

				<xsl:attribute name="title">Responsable : <xsl:value-of select="@resp"/></xsl:attribute>
	<xsl:apply-templates/>                    

	</p>
	</xsl:template>
	<xsl:template match="tei:note[@place='margin-left']" >
		<p>
	  	<xsl:attribute name="class">manchette_gauche</xsl:attribute>
					<xsl:attribute name="title">Responsable : <xsl:value-of select="@resp"/></xsl:attribute>
			<xsl:apply-templates/>                    
		</p>
	</xsl:template>     
    
	<xsl:template match="tei:note[@place='ligneSub']" >
		<p>
	  	<xsl:attribute name="class">manchette_gauche</xsl:attribute>
					<xsl:attribute name="title">Place : Au-dessous de la ligne. Responsable : <xsl:value-of select="@resp"/></xsl:attribute>
			<xsl:apply-templates/>                    
		</p>
	</xsl:template>  	
	<xsl:template match="tei:note[@place='ligneSup']" >
		<p>
	  	<xsl:attribute name="class">manchette_droite</xsl:attribute>
					<xsl:attribute name="title">Place : Au-dessus de la ligne. Responsable : <xsl:value-of select="@resp"/></xsl:attribute>
			<xsl:apply-templates/>                    
		</p>
	</xsl:template>  		
		
		
	<!-- Notes d'identification de manuscrit -->
	<xsl:template match="//tei:note[@type='identification']" >
		<xsl:variable name="numNote">
			<xsl:number count="tei:note[@type='identification']" level="any" from="/" format="1"/>
		</xsl:variable>	
		<xsl:text> </xsl:text>
		<div class="affichePlus identification_ms ui-corner-all">
			<span id="spanNote{$numNote}"  class="plus">
				<a><xsl:attribute name="onclick">toggleNote('spanNote<xsl:value-of select="$numNote"/>','note<xsl:value-of select="@type" /><xsl:value-of select="$numNote"/>');</xsl:attribute>[+]</a>
			</span>
			<span id="note{@type}{$numNote}" class="noteNon">
			<a class="noteMoins"><xsl:attribute name="onclick">toggleNote('spanNote<xsl:value-of select="$numNote"/>','linkNote<xsl:value-of select="$numNote"/>');</xsl:attribute>[-]</a><xsl:text> </xsl:text><xsl:apply-templates/>
			</span>
			<span id="linkNote{$numNote}" class="notePlus">
					<a><xsl:attribute name="onclick">toggleNote('spanNote<xsl:value-of select="$numNote"/>','note<xsl:value-of select="@type" /><xsl:value-of select="$numNote"/>');</xsl:attribute>[+]</a>
			</span>
		</div>
	</xsl:template>
		
		<!-- <xsl:template match="//tei:note[@type='identification']/tei:p" ><div class="identification"><xsl:apply-templates/></div></xsl:template> -->
		
	<xsl:template match="//tei:note[@type='conservation']" >
		<p class="conservation"><xsl:apply-templates/></p>
	</xsl:template>
	<xsl:template match="//tei:note[@type='attestation']" >
		<xsl:choose>
			<xsl:when test="text()">
		<p class="attestationP"><xsl:apply-templates/></p>
	</xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
	</xsl:template>
    
</xsl:stylesheet>