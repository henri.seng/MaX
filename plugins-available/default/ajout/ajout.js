
import {Plugin} from '../../core/ui/js/Plugin.js';

const ADD_CLASS = "add";
const DEL_CLASS = "del";

class AjoutPlugin extends Plugin{
  constructor(name) {
    super(name);
  }

  run(){
    console.log("Plugin ajout running")
    var checkAttr = "";
    if(localStorage.
           getItem(MAX_LOCAL_STORAGE_PREFIX + ADD_CLASS) === MAX_VISIBLE_PROPERTY_STRING)
    {
      checkAttr = "checked='checked' ";
      //updates visibility
      this.on();
    }
    else{
      //updates visibility
      this.off();
     }

    $("#options-list").append("<li><a><input id='toggle_add' type='checkbox' "
          +checkAttr
          +" name='toggle_add'>Afficher les interventions</a></li>");
    let self = this;
    $('#toggle_add').change(function(){
      self.setExVisible()
    })
  }

  setExVisible(){
    if($('#toggle_add').is(":checked"))
    {
      this.on();
      localStorage.setItem(MAX_LOCAL_STORAGE_PREFIX + ADD_CLASS, MAX_VISIBLE_PROPERTY_STRING);
    }
    else{
      this.off();
      localStorage.removeItem(MAX_LOCAL_STORAGE_PREFIX + ADD_CLASS);
    }
  }


  /* la balise add est mise en vert et la balise del est affichée */
  on(){
  $("." + ADD_CLASS).css("color","green");
   $("." + DEL_CLASS).show();
    $("." + DEL_CLASS).css("color","initial");
  }
  /* la balise add est affichée mais la balise del est masquée */
  off(){
     $("." + ADD_CLASS).css("color","inherit");
     $("." + DEL_CLASS).hide();
  }
}

MAX.addPlugin(new AjoutPlugin('Ajout'));