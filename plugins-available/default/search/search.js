import {Plugin} from '../../core/ui/js/Plugin.js';

const ALL_TXT_SEARCH = 'all_txt';


class SearchPlugin extends Plugin{
    constructor(name) {
     super(name);
     if(document.querySelector('input[name="searchMode"]'))
        this.searchModeChanged();
  }
  
  
  runSearch(search, docs, transform, divToFeed){
    let self = this;
     $.post(
          window.MAX.getBaseURL() + window.MAX.getProjectID() + "/search",
          {
            search: search,
            docs: docs,
            transform: transform
          },
          function(data){
            divToFeed.html(data);         
            self.updateHitsInfo(docs);
          }  
      );  
   }

   searchModeChanged(){
        this.searchMode = document.querySelector('input[name="searchMode"]:checked').value;
        if(this.searchMode === ALL_TXT_SEARCH){
            document.getElementById('searchSelect').classList.add('d-none');
        }
        else {
            document.getElementById('searchSelect').classList.remove('d-none');
        }
   }
   
   
   
  /*
  Performs search according to form parameters
  */
  runSearchFromForm(){
    let search = $('#searchInput').val();
    if(search.trim() === ''){
      window.alert('Recherche vide ou incorrecte.');
      return;
    }
    $("#searchLoading").show();

    let docs = $('#searchSelect').val();
    let transform = $('#searchTransform').prop('checked') ?'yes' : 'no'

    this.runSearch(search, this.searchMode === ALL_TXT_SEARCH ? [] : docs, transform, $("#searchResults"));
    
  }


  updateHitsInfo(){
    $('.manchette_droite').remove();
    $(".hit").each(function(){
      $(this).prepend("<span class='hit_range'>["+ ($(this).prevAll(".hit").length+ 1) +"]</span>")
    });
    $("#searchResults").prepend("<h3 class='n-search-result'>"+$('.hit').length+" résultat(s)</h3>")
    $("#hits").css('visibility', 'visible');
    $("#searchLoading").hide();

  }
}

let plugin = new SearchPlugin('Search');
window.MAX.addPlugin(plugin);
window.search = plugin