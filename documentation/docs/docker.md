# Docker

* L'image ne peut être créée que si une édition existe (la création d'une image à partir d'un MaX vide est impossible, et inutile !).

* Création de l'image de l'édition (exemple avec l'édition de démonstration) puis lancement du container accessible sur le port 9999 (base XML du projet stockée dans */opt/basex/data/max_demo_lorem* :

Création de l'édition de démonstration

```
$ ./tools/max.sh -i && ./tools/max.sh -d

```

Ajout des droits sur les données de la base pour le container :

```
$ sudo chown -R 1984 /opt/basex/data/max_demo_lorem
```

Création de l'image de l'édition puis lancement du container :

```
 $ sudo docker build -t unicaen/max_demo_lorem .
 $ sudo docker run -d -p 9999:8984 --volume /opt/basex/data/max_demo_lorem:/srv/basex/data/max_demo_lorem unicaen/max_demo_lorem
```
