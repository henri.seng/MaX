# Fonctionnalités et URLs associées


* `/[edition]/accueil` Page d'accueil d'une édition
* `/[edition]/sommaire` Sommaire d'une édition (liste des documents XML consultables)
* `/[edition]/sommaire/[document].xml` Sommaire d'un document d'une édition
* `/[edition]/[document].xml/[id]` Consultation d'un fragment identifié, d'un document d'une édition
* `/[edition]/doc/[document].xml/[id]` Consultation d'un document d'une édition
* `/[edition]/[page]` Consultation d'un contenu html statique

# Chaîne de traitement

Lors de la consultation d'un sommaire, d'un document ou d'un fragment, MaX effectue les opérations suivantes :

1. requêtage XQUERY pour récupération des données (liste de document, document complet, fragment identifié, etc.)
2. Transformation de ces données en un contenu HTML par application de templates XSLT
3. Création d'une page HTML complète (à partir d'un template) contenant les données transformées, les blocs de navigation, menu, etc. ainsi que les imports CSS et Javascript
nécessaires.
4. Exécution des éventuels plugins Javascript au sein du navigateur
