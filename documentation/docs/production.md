# Mise en production

Pour une instance MaX déployée sur http://localhost:1234/

## Base URI

Modifier dans le fichier de configuration le base URI correspondant à l'URL de mise en production.
Par exemple, pour un MaX accessible à https://www.unicaen.fr/max-demo :

```
 <baseURI>/max-demo/</baseURI>
```

## Configuration Apache
Sur le serveur de production, ajouter les règles de reverse-proxy permettant de joindre votre instance de MaX disponible 
localement.

Par exemple, pour un MaX actif sur à http://localhost:1234 :

```
    ProxyPass /max-demo/demo_lorem/ http://127.0.0.1:1234/demo_lorem/
    ProxyPassReverse /max-demo/demo_lorem/ http://127.0.0.1:1234/demo_lorem/

    ProxyPass /max-demo/plugins/ http://127.0.0.1:1234/plugins/
    ProxyPassReverse /max-demo/plugins/ http://127.0.0.1:1234/plugins/

    ProxyPass /max-demo/core/ http://127.0.0.1:1234/core/
    ProxyPassReverse /max-demo/core/ http://127.0.0.1:1234/core/
```

une seule règle ```/max-demo/``` vers ```http://127.0.0.1:1234/``` rendrait l'application d'administration BaseX accessible depuis l'extérieur
à l'adresse https://www.unicaen.fr/max-demo/dba/.