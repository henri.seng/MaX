# Mise en page

Le template de mise en page par défaut se trouve dans **ui/templates/[env].html**.
Il est possible de remplacer cette mise en page en créant son propre template et en le convoquant dans la configuration de l'édition concernée. Voici un exemple,
pour une mise en page avec un menu vertical à gauche sur l'édition de démonstration :

```
<layout template="editions/demo_lorem/ui/layout/template.html"/>
```

Il est nécessaire d'activer la surcharge de transformation du menu en renommant le fichier **editions/demo_lorem/ui/xsl/_menu_left.xsl** en **editions/demo_lorem/ui/xsl/menu.xsl**.

