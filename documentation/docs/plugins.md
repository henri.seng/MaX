# Plugins

MaX propose un ensemble de plugins placés dans le répertoire *plugins-enabled/default*.
Tout autre plugin devra être placé dans *plugins-enabled/custom*.

Le script *tool/max.sh* permet l'activation/désactivation de ces plugins.

````
# lister les plugins disponibles
$ ./tools/max.sh --list-plugins

# Activer un plugin (devient alors actif et disponibles pour l'ensemble des éditions/projet de l'instance MaX)
./tools/max.sh --enable-plugin <plugin_name>

# Désactiver un plugin
./tools/max.sh --disable-plugin <plugin_name>

````

Une fois activé, un plugin doit être déclaré (et configuré si nécessaire) dans les fichiers de configuration des éditions l'utilisant.
Déclaration et configuration se font au sein d'un élément `<plugin/>` de la section `<plugins/>` du fichier de configuration d'une édition.

## Les plugins disponibles

### Abréviations

### Ajouts

### Apparat critique
Gestion de l'affichage de l'apparat critique :

```xml
<plugin name="apparat_critique"/>
```

### Fil d’Ariane (Breadcrumb)

Affichage d'un fil d'Ariane lors de la consultation du texte. Le paramètre `topLabel` permet de configuré
le label de la racine du fil d'Ariane.

```xml
 <plugin name="breadcrumb">
   <parameters>
     <parameter key="topLabel" value="Démo. Max [TEI]"/>
   </parameters>
 </plugin>
```

### Corrections

Affichage/masquage des erreurs et de leurs corrections (balises `tei:sic` & `tei:corr`).

```xml
<plugin name="correction"/>
```

### Équations

Permet l'affichage HTML des équations mathématiques (encodage `tei:formula[@notation='TeX']`)

```xml
<plugin name="equations"/>
```

### Visionneuse d'images (img_viewer)

Permet la consultation des images au sein d'une visionneuse ([Openseadragon](https://openseadragon.github.io/)). 

Ce plugin requiert le chemin (relatif à l'édition) de stockage des images (`imagesRepository`) comme paramètre de configuration :

```
     <plugin name="img_viewer">
       <parameters>
         <!--@xsl(default:false): added to xslt parameters map if true -->
         <parameter key="imagesRepository" value="ui/images/" xsl="true"/>
       </parameters>
     </plugin>
```

Pour consulter des images tuilées (dzi, iiif, etc.), il fat surcharger le template `tei:graphic` (dans la `text_hook` de l'édition)
afin de remplacer le paramètre d'appel à `MAX.plugins['img_viewer'].openImageInDialog` par l'URL du `.dzi` ou `.json`.


### Pagination (Pager)

### Recherche (search)

Voici un exemple de configuration du plugin de recherche :

```xml
<plugin name="search">
	<parameters>
		<!--Les recherches sont effectuées dans toutes les balises p (et leurs descendants)-->
        <parameter key="tag" value="p"/>
		<!--Pour chacun des résultats, le lien de retour au texte pointe
		vers la div ancêtre identifiée ayant un @ type-->
        <parameter key="backToTextID" value="(./ancestor::*:div[@*:type])[1]/@xml:id"/>
    </parameters>
</plugin>
```

 * **tag**: nom du tag xml dans lesquels les recherches sont effectuées
 * **backToTextId**: xpath à suivre pour les liens de retour au texte sur les éléments de résultat
 
 
## Plugins et dépendances Javascript
 
Certains plugins ont des dépendances vers des librairies js (ex : *img_viewer* requiert *OpenSeadragon*).
Ces librairies sont automatiquement copiées à l'activation du plugin à la lecture
du fichier *plugins/<plugin_name>/resources.json*.

Ex : l'édition de démo fonctionnant avec les plugins *img_viewer* et *equations*, les librairies *Mathjax* et *Openseadragon* sont
automatiquement copiées lors de l'installation de l'édition (lecture de *equations/dependencies.json* et *img_viewer/resources.json*).

Note : ces dépendances ne sont pas supprimées lors de la désactivation d'un plugin (à venir ?) afin d'éviter la suppresion d'une lib
utilisée par un autre plugin.

## Récupération d'éléments hors contexte

documentation todo :
  - xquery propre à un plugin <nom_plugin>.xq (cas de la visionneuse d'images)
  - ou plugin headerPicker -> todo (qui prend un xpath en paramètre. Voir https://git.unicaen.fr/pdn-certic/MaX/-/issues/75)