# Sommaires

## Table des matières d'une édition
La table des matières d'une édition est disponible à l'URL **http://[host]:[port]/[project]/sommaire**
Par exemple :
```
http://localhost:1234/pouille/sommaire
```
Par défaut, la page de sommaire liste les documents présents dans la collection, par ordre alphabétique :

* pour la TEI, les éléments : `/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title`
* pour l'EAD, les éléments  : @todo


### Surcharge du comportement

Il est possible de :

* Modifier la XQuery afin de modifier les entrées listées dans la table des matières (ou leur ordre de tri)
* Modifier la XSL transformant cette liste des entrées au format HTML

#### Modification de la XQuery

1. Créer un fichier editions/[projectId]/xq/toc.xq
2. Y rédiger une requête Xquery prenant 3 paramètres
    * $baseURI : valeur de la variable de configuration [baseURI](../config#la-variable-baseuri)
    * $dbPath  : valeur de la variable de configuration [dbPath](../config) permettant le requêtage la base de données
    * $project : identifiant de l'édition

Exemple de squelette d'une telle XQuery :

```
declare variable $baseURI external;
declare variable $dbPath external;
declare variable $project external;


<ul>
  [...]
<ul>
```  

Afin d'être compatible avec la XSL de la table des matières de MaX (fichier MAX/ui/xsl/toc.xsl), cette XQuery doit générer
une balise `<ul/>` composée d'élements `<li/>` et `<ul/>`. Les items de lien devront renseigner ces liens dans l'attribut **data-href**.

Exemple de listing (généré par la XQuery par défaut de MaX) compatible avec **MAX/ui/xsl/toc.xsl** :

```
<ul>
  <li data-depth="0" data-href="/demo_lorem/sommaire/demo_align_fr.xml">
    <title xmlns="http://www.tei-c.org/ns/1.0">Lipsum aligné - Version française / traduction latine.</title>
  </li>
  <li data-depth="0" data-href="/demo_lorem/sommaire/demo_align_lat.xml">
    <title xmlns="http://www.tei-c.org/ns/1.0">Lipsum - Version Latine.</title>
  </li>
  <li data-depth="0" data-href="/demo_lorem/sommaire/demo_lorem.xml">
    <title xmlns="http://www.tei-c.org/ns/1.0">Lorem Ipsum <lb></lb>In <hi rend="small-caps">MaX</hi> - Corrections, notes &amp; sauts de page</title>
  </li>
  <li data-depth="0" data-href="/demo_lorem/sommaire/demo_lorem_2.xml">
    <title xmlns="http://www.tei-c.org/ns/1.0">Lorem Ipsum In MaX - Tome 2. Apparat critique</title>
  </li>
  <li data-depth="0" data-href="/demo_lorem/sommaire/demo_lorem_3.xml">
    <title xmlns="http://www.tei-c.org/ns/1.0">Lorem Ipsum in MaX - Tome 3. Images et Équations</title>
  </li>
</ul>
```

#### Modification de la XSL


Créer un fichier **editions/[projectId]/ui/xsl/toc.xsl** afin de transformer le résultat de la XQuery
(XQuery par défaut ou surcharge **editions/[projectId]/xq/toc.xq** ) en HTML.

En l'absence d'un tel fichier, MaX exécutera **MAX/ui/xsl/toc.xsl**.


## Table des matière d'un document
La table des matières d'un document est disponible à l'URL **http://[host]:[port]/[project]/sommaire/[document_path].xml**
Par exemple :
```
http://localhost:1234/pouille/sommaire/pouille1.xml
```

Par défaut (en TEI), MaX liste les balises `<head/>` des div identifiées (avec un attribut @xml:id) contenant un attribut @type.

### Surcharge du comportement

Tout comme la table des matières d'une édition, il est possible de :

* Modifier la XQuery afin de modifier les éléments sélectionnés pour la table des matières
* Modifier la XSL transformant ces éléments au format HTML

#### Modification de la XQuery

1. Créer un fichier editions/[projectId]/xq/document_toc.xq
2. Y rédiger une requête Xquery prenant 4 paramètres
    * $baseURI : valeur de la variable de configuration [baseURI](../config#la-variable-baseuri)
    * $dbPath  : valeur de la variable de configuration [dbPath](../config)
    * $project : identifiant de l'édition
    * $doc     : nom du document courant pour lequel est construit la table des matières

Exemple de squelette d'une telle XQuery :

```

declare variable $baseURI external;
declare variable $dbPath external;
declare variable $project external;
declare variable $doc external;

(: xquery listant les items du sommaire du document $doc:)
<ul>
    {
      for $entry in doc($dbPath || '/' || $doc)// ...
      return     
        <li id="..." data-href="..."><head>...</head></li>
    }
</ul>

```  

Afin d'être compatible avec la feuille de transformation XSL appliquée par défaut au résultat de cette XQuery, celle-ci doit :

* retourner le sommaire dans un élement tei `<ul/>`
* construire des sous-éléments `<li/>` pour chaque entrée en spécifiant l'url des liens dans un attribut data-href
* construire un sous-élement `<head/>` pour chaque `<li/>`, contenant le libellé de l'entrée.

#### Modification de la XSL


Créer un fichier **editions/[projectId]/ui/xsl/document_toc.xsl** afin de transformer le résultat de la XQuery
(XQuery par défaut ou surcharge **editions/[projectId]/xq/document_toc.xq** ) en HTML.
Cette XSL reçoit en entrée

* le titre du document courant dans une variable **$docTitle**
* les variables de configuration **$baseuri** et **$project"**.


En l'absence d'un tel fichier, MaX exécutera **MAX/ui/xsl/document_toc.xsl**.
