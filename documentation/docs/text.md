# Textes

La consultation des textes se fait aux URLs suivantes :

* `http://[host]:[port]/[project]/[chemin_fichier.xml]/[id]` : consultation d'un fragment identifié
* `http://[host]:[port]/[project]/doc/[chemin_fichier.xml]` : consultation d'un document XML complet
* `http://[host]:[port]/[project]/fragment_html/[id]` : consultation "isolée" (sans css, js, menu, barre de navigation, etc.). L'ajout du query parameter
`?wrap=true` offre une consultation dans une page HTML complète (css + js).

La feuille de transformation appliquée par défaut se trouve dans **ui/xsl/[environnement]/[environnement].xsl** (environnement = tei ou ead).


Lors de l'affichage du texte d'un fragment, la barre de navigation (liste déroulante) permet de naviguer de fragment en fragment.
Les entrées de cette liste déroulante sont par défaut identiques aux entrées de la table des matières du document en cours
de consultation.
La surcharge de cette transformation se fait en rédigeant une XSL placée dans **editions/[projectId]/xsl/text_hook.xsl**.

Aussi, les XSL des [plugins](../plugins) activés respectant la convention de nommage **plugins-enabled/NOM_PLUGIN/NOM_PLUGIN.xsl** sont automatiquement appliquées.


## Barre de navigation


![Barre de navigation](images/nav_text.png)

Lors de l'affichage du texte d'un fragment, la barre de navigation (liste déroulante) permet de naviguer de fragment en fragment.
Les entrées de cette liste déroulante sont par défaut identiques aux entrées de la table des matières du document en cours
de consultation.
Il est possible de modifier l'affichage des ces entrées en ajoutant une feuille de transformation dans **editions/[projectId]/xsl/nav_bar.xsl**.

Celle-ci recevra en entrée le même arbre XML que **editions/[projectId]/xsl/document_toc.xsl** ainsi que les  paramètres **$baseuri**, **$project** accompagnés de :

* **$selectedId** : identifiant du fragment en cours de consultation
* **$nextArrow**  : 'true' s'il existe des fragments suivants le fragment courant
* **$prevArrow**  : 'true' s'il existe des fragments précédents le fragment courant


## Options de lecture


## Alignement
