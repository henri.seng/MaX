# Configuration


## Fichiers de configuration

### Configuration générale de MaX
Le fichier *configuration/configuration.xml* inclut les fichiers de configuration des différentes éditions
gérées par MaX.


````
<!-- Exemple de fichier de configuration de MaX incluant 2 éditions : demo_lorem et mon_edition-->

<?xml version="1.0"?>
<configuration xmlns:xi="http://www.w3.org/2001/XInclude">
  <baseURI/>
  <editions>
    <xi:include href="../editions/demo_lorem/demo_lorem_config_inc.xml"/>
    <xi:include href="../editions/mon_edition/mon_edition_config_inc.xml"/>
    [...]
  </editions>
</configuration>

````

### Configuration d'une édition

L'édition *my_edition* sera configurée dans le fichier *editions/mon_edition/mon_edition_config_inc.xml*.

```
<!-- Exemple de fichier de configuration par défaut, sans option ni surcharge si plugin-->

<edition xml:id="mon_edition" dbpath="moneditiondb" env="tei">
</edition>
```

* @xml:id : identifiant de l'édition. Toutes les urls de consultation commencent par cet id
* @dbpath : Nom de la base de données contenant les sources consultables
* @env    : Grammaire XML de l'édition


Ce fichier contiendra si nécessaire, la [configuration des options de lecture](../reading_options), l'activation et le paramètrage de
[l'alignement](../alignment) et des [plugins](../plugins).

#### La variable baseURI

Pour un site déployé sur une URL autre que la racine de l'hôte, la variable *baseURI* doit être renseignée dans le fichier
*configuration/configuration.xml*. Par exemple, pour un site publié à l'adresse https://site.domain.ext/app/ :

```
<?xml version="1.0"?>
<configuration xmlns:xi="http://www.w3.org/2001/XInclude">
  <baseURI>/app/</baseURI>
  [...]  
</configuration>
```