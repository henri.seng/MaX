# Menu

## Le fichier menu.xml

Le menu d'une édition **mon_edition** se configure dans un fichier **editions/mon_edition/menu.xml** décrivant au maximum deux niveaux de menu . 
Pour une entrée de menu, il est nécessaire de renseigner son identifiant ainsi que sa cible. Dans l'exemple ci-contre, 
le menu généré contiendra deux entrées principales (attribut type='main') : *home* et *toc*.

```
<menu>
  <entry type="main" default="true">
    <id>home</id>  
    <target>accueil</target>

    <!--home sub entries-->
    <entry>
        <id>presentation</id>
        <target>presentation</target>
    </entry>
    <entry>
        <id>contacts</id>
        <target>contacts</target>
    </entry>
  </entry>

  <entry type="main">
    <id>toc</id>
    <target>sommaire</target>
  </entry>

</menu>
```

L'entrée home pointera vers l'URL **/mon_edition/accueil** et contiendra deux sous-entrées **presentation** et **contacts** pointant respectivement vers **/mon_edition/presentation** et **/mon_edition/contact**.

## Modification de la mise en forme du menu

Le menu HTML est le résultat de la transformation du fichier **mon_edition/menu.xml** par la feuille XSL par défaut placée dans **{MAX-dir}/ui/menu.xsl**.
Une feuille de transformation placée dans **mon_edition/ui/xsl/menu.xsl** remplacera celle par défaut. 3 paramètres sont disponibles dans cette XSL :

* *projectId* : identifiant du projet
* *baseURI* : baseURI du projet
* *selectedTarget* : entrée de menu courante (valeur d'un des attributs @target)
