# Création d'édition

## Déploiement de l'édition de démonstration

* `cd tools` - Se déplacer dans le dossier *tools*.
* `export BASEX_PATH=/path/to/basex` : préciser l'emplacement d'installation de BaseX si la commande *basexclient* n'est pas déjà dans votre PATH.
* Les commandes suivantes peuvent prendre l'argument `-p` avec un numéro de port afin d'alimenter BaseX s'il tourne sur un port différent de celui par défaut
(1984).

*Attention à ne pas confondre le port de la base de données (1984 par défaut) avec le port du service HTTP (8984 par défaut).*

```
$ ./max.sh -d

ou (pour un BaseX tournant sur le port 1234)

$ ./max.sh -p 1234 -d
```

L'édition de démonstration est consultable à [http://localhost:8984/demo_lorem](http://localhost:8984/demo_lorem)


## Déploiement d'une nouvelle édition

```
# Utilisation du port par défaut de BaseX : 1984
$ ./max.sh -n

ou

./max.sh -p 1234 -n
```

Plusieurs questions sont posées :

	1. Identifiant du projet ?

Saisissez le nom de site, sans espace ni caractères spéciaux. Exemple : pouille

	2. Nom de la base de données ?

  Renseignez le nom de la base de données XML

	3. Vocabulaire XML du projet (tei, ead, ...)?

  Saisissez tei ou ead

	4. Dossier des sources XML à charger ?

  Resneigner le chemin complet vers le dossier où se trouvent les sources XML. Exemple : /Users/johndoe/Documents/fichiersBaseX/pouille

	5. Please type your BaseX login/password :

Par défaut admin / admin

- Username: admin
- Password: admin

Vous pouvez enfin vous connecter en local à l’adresse :
`http://localhost:[numero_de_port]/NOM_PROJET`

Exemple : http://127.0.0.1:8984/pouille

### Activation de plugins

voir [plugins](/plugins)
