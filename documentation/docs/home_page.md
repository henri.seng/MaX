# Page d'accueil

La page d'accueil d'une édition est accessible depuis
```
http://[host]:[port]/[project]/accueil
```

Par exemple :

```
http://localhost:1234/pouille/accueil
```

Éditer le fichier **editions/[project]/fragments/accueil.frag.html** pour la modifier.


Par défaut, l'URL *http://[host]:[port]/[project]/* redirige automatiquement vers *http://[host]:[port]/[project]/accueil*