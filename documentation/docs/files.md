# Organisation des fichiers



* **configuration**: contient le/les fichiers de configuration
* **documentation**: contient la documentation de MaX
* **editions**: dossier des *éditions* (ou *projets*) hébergées par l'instance de MaX. Chaque édition à son propre dossier dans lequel sont stockés ses ressources HTML, XSLT, CSS, Javascript, XQuery, etc.
* **node_modules**: dossier des dépendances node.js installées. Ce dossier n'est créé qu'a l'initialisation de MaX (ou lors d'un ```npm install```)
* **plugins-available**: dossier des plugins disponibles. Chaque plugin dispose de son propre dossier de ressources: fichiers Javascript, XQuery, XSL, ...
* **plugins-enabled**: dossier des plugins activés (liens symboliques vers *plugins-available*)
* **rxq**: ensemble des modules RestXQ
* **tools**: outils de déploiement de l'édition de démonstration et d'une nouvelle édition
* **ui**
    * **css** : feuilles de style natives de MaX
    * **i18n** : ressources d'internationalisation
    * **images**
    * **js** : sources javascript de MaX
    * **lib** : librairies externes
    * **templates** : templates HTML
    * **xsl**	: feuilles de transformations XSL natives de MaX
* **package.json**: fichier des dépendances javascript
* **max.xq**: "contrôleur" RestXQ de l'application MaX
