# Avant-propos

Cette documentation en cours de rédaction concerne la branche [dev de l'application MaX](https://git.unicaen.fr/pdn-certic/MaX/-/tree/dev).

# Installation

## Prérequis

* Java 8+
* NodeJS (et npm) 10+
* xmllint
* BaseX 9.2+

## Commandes

* `git clone -b dev https://git.unicaen.fr/pdn-certic/MaX.git` Récupération du code source
* `cd tools && ./max.sh -i` Initialisation de MaX, installation des dépendances
* `cd </path/to/basex>/webapp`
* `sudo ln -s /path/to/max` création d'un lien symbolique pointant vers le dossier d'installation de MaX
* `cd </path/to/basex>/bin`
* `./basexhttp` Lancement du serveur HTTP de BaseX


Vérifiez l'installation en consultant [http://localhost:8984/max](http://localhost:8984/max)
