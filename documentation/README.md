# Documentation Max


## Édition
* Nécessite d'installer [Mkdocs](https://www.mkdocs.org)
* Lancer la commande `mkdocs serve`

## Déploiement

Statification + mise en ligne :

```
$ mkdocs build
$ scp -r ./site user@host:/path/to/server
```


## Version en ligne

Documentation en cours de rédaction disponible [ici](https://www.certic.unicaen.fr/documentations/max/)