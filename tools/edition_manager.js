//checks dependencies availabilty
var execSync;
var fs;
try {
    require.resolve("readline");
    require.resolve("xmldom");
    execSync = require('child_process').execSync;
    fs = require('fs');

} catch (e) {
    console.log("Missing librairies - Please run npm install readline xmldom child_process");
    process.exit(e.code);
}

//imports des librairies
var fs = require('fs');
var readline = require('readline');
var DOMParser = require('xmldom').DOMParser;
var XMLSerializer = require('xmldom').XMLSerializer;


const CONFIGURATION_FOLDER_PATH = __dirname + "/../configuration/";
const EDITIONS_FOLDER_PATH = __dirname + "/../editions/";
const MAIN_CONFIG_FILE_NAME = "configuration.xml";
const MAIN_CONFIG_FILE = CONFIGURATION_FOLDER_PATH + MAIN_CONFIG_FILE_NAME;
const TEMPLATE_CONFIG_FILE = __dirname + "/edition_conf_tmpl.xml";
const DEMO_CONFIG_INC = EDITIONS_FOLDER_PATH + "demo_lorem/demo_lorem_config_inc.xml"


if (process.argv[2] == "-demo")
    include_baseconf("demo_lorem", DEMO_CONFIG_INC);
else if (process.argv[2] == "-new")
    new_edition(process.argv[3],process.argv[4],process.argv[5]);
else if (process.argv[2] == "-plugin")
    install_plugin_dependencies(process.argv[3]);
return;


function new_edition(editionId, db, type) {
    create_project_baseconf(editionId, db, type);
}

/*
  Substitution des variables de l'édition au sein du template
  de base.
*/
 function create_project_baseconf(id, db, type_env) {
    var template = fs.readFileSync(TEMPLATE_CONFIG_FILE, {encoding: 'utf-8'})
    template = template.replace("%ID%", id).replace("%DB%", db).replace("%ENV%", type_env);
    //Écriture du fichier de config.
    var filename = id + "_config_inc.xml";
    fs.writeFileSync(filename, template);
    if (!fs.existsSync(EDITIONS_FOLDER_PATH + id)) {
        fs.mkdirSync(EDITIONS_FOLDER_PATH + id, (err) => {
            if (err) console.log(err);
        });
    }
    //Déplacement du fichier créé dans le répertoire de l'édition.
    fs.rename(filename, EDITIONS_FOLDER_PATH + id + "/" + filename, function (err) {
        if (err) {
            return console.log(err);
        }
    });
    console.log("=== Création du fichier de configuration " + filename + " - OK");
    include_baseconf(id, EDITIONS_FOLDER_PATH + id + "/" + filename)
}

/*
Inclusion d'une configuration d'une édition au sein de la configuration globale de MaX
*/
 function include_baseconf(project_id, config_file) {
    try {
        var relative_file = config_file.split(__dirname +'/')[1]
        var include_content = "<xi:include href='" + relative_file + "' xmlns:xi='http://www.w3.org/2001/XInclude'/>";
        var config_content = fs.readFileSync(config_file, {encoding: 'utf-8'})
        var include_dom = new DOMParser().parseFromString(include_content);
        install_plugins_dependencies(new DOMParser().parseFromString(config_content));
        var main_config_content = fs.readFileSync(MAIN_CONFIG_FILE, {encoding: 'utf-8'})
        var config_dom = new DOMParser().parseFromString(main_config_content);
        var editions_node =
            config_dom.getElementsByTagName('configuration')[0].getElementsByTagName('editions')[0];
        let existing_includes = config_dom.getElementsByTagNameNS("http://www.w3.org/2001/XInclude","include");
        for(let i = 0; i < existing_includes.length; i++){
          let href = existing_includes[i].getAttribute('href');
          if(href === relative_file){
            console.log("\n[WARNING] Le projet " + project_id +" est déjà importé dans le fichier de configuration principale.\n");
            return true;
          }
        }
        var a = config_dom.importNode(include_dom, 1);
        editions_node.appendChild(a);
        var s = new XMLSerializer();

        //Le résultat de la sérialisation n'est pas correctement indentée
        fs.writeFileSync(MAIN_CONFIG_FILE, s.serializeToString(config_dom))//, function (err) {
        console.log("=== Mise à jour de la configuration principale de MaX (inclusion de " + relative_file + ") - OK");
        return true;

    } catch (e) {
        console.log(e)
    }

}

/*
* Installation des plugins
*/
function install_plugins_dependencies(dom){
    let pluginElts = dom.getElementsByTagName('plugins')[0].getElementsByTagName('plugin');
    if(pluginElts.length > 0)
      console.log("==== Installation des dépendances JS des plugins [" + pluginElts.length +"]")
    else {
      console.log("==== Aucun plugin à traiter")
    }
    for(let i = 0; i < pluginElts.length; i++){
        let name = pluginElts[i].getAttribute('name');
        install_plugin_dependencies(name);
//        console.log()
//        let path = __dirname+'/../plugins-enabled/' + name +'/resources.json';
//        if (fs.existsSync(path)) {
//          console.log(' + Installation des dépendances pour ' + name);
//          let data = fs.readFileSync(path);
//          let deps = JSON.parse(data);
//          Object.keys(deps).forEach(function(k,v){
//            console.log(' cp -r ' + __dirname + '/../'+deps[k] +' ' +__dirname + '/../ui/lib/');
//            execSync('cp -r ' + __dirname + '/../'+deps[k] +' ' + __dirname + '/../ui/lib/');
//          });
//        }
//        else{
//          console.log(' - Pas de dépendances pour ' + name);
//        }
    }
}

function install_plugin_dependencies(plugin_id){
        let path = __dirname+'/../plugins-enabled/' + plugin_id +'/resources.json';
        if (fs.existsSync(path)) {
          console.log(' + Installation des dépendances pour ' + plugin_id);
          let data = fs.readFileSync(path);
          let deps = JSON.parse(data);
          Object.keys(deps).forEach(function(k,v){
            console.log(' cp -r ' + __dirname + '/../'+deps[k] +' ' +__dirname + '/../ui/lib/');
            execSync('cp -r ' + __dirname + '/../'+deps[k] +' ' + __dirname + '/../ui/lib/');
          });
        }
        else{
          console.log(' - Pas de dépendances pour ' + plugin_id);
        }
}
