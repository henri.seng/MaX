#!/bin/bash

PORT=1984
DIRECTORY=$(cd `dirname $0` && pwd)
AVAILABLE_PLUGINS_DIR=$DIRECTORY/../plugins-available
ENABLED_PLUGINS_DIR=$DIRECTORY/../plugins-enabled
MAX_PLUGINS_DIR=$AVAILABLE_PLUGINS_DIR/default
CUSTOM_PLUGINS_DIR=$AVAILABLE_PLUGINS_DIR/custom
RELATIVE_MAX_PLUGINS_DIR=../plugins-available/default
RELATIVE_CUSTOM_PLUGINS_DIR=../plugins-available/custom
BASEX_CLIENT_BIN="basexclient"
BASEX_BIN="basex"


#checks and set basex bin
a=$(command -v basexclient)

if [ -z $a ] && [ -z $BASEX_PATH ]
then
   echo "Please install BaseX or set \$BASEX_PATH environment variable"
   exit 1
else
   if [ -z $a ]
   then
      BASEX_CLIENT_BIN=$BASEX_PATH"/bin/basexclient"
      BASEX_BIN=$BASEX_PATH"/bin/basex"
   fi
fi

display_usage(){
    echo ""
    echo "   MaX - Utilities"
    echo ""
    echo "   -h: Display help."
    echo "   -p: Specify BaseX port to use for db feed. Default one is "$PORT"."
    echo "   -i: Max Javascript env initialization."
    echo "   -d: Deploy the demo edition project."
    echo "   -n: Deploy new edition with its XML sources."
    echo "   --list-plugins: show plugins and status."
    echo "   --enable-plugin [plugin_name]: Enable [plugin_name] plugin."
    echo "   --disable-plugin [plugin_name]: Disable [plugin_name] plugin."
    echo ""
}

list_plugins(){
  echo -e ''
  echo -e 'MaX default plugins : '
  list_plugins_dir $MAX_PLUGINS_DIR
  echo -e ''
  echo -e 'MaX custom plugins : '
  list_plugins_dir $CUSTOM_PLUGINS_DIR
  echo -e ''
}

list_plugins_dir(){
  for i in `find $1/* -maxdepth 1 -type d`
  do
    dir=`basename $i`
    if [ -d $ENABLED_PLUGINS_DIR/$dir ]
    then
      status="ENABLED"
    else
      status="DISABLED"
    fi
    echo '   - '$dir' -> '$status
  done
}

enable_plugin(){
  echo -e ''
  if [ -d $ENABLED_PLUGINS_DIR/$1 ]
  then
      echo -e "Plugin $1 was already enabled."
      return
  fi

  if [ -d $MAX_PLUGINS_DIR/$1 ]
  then
    target=$RELATIVE_MAX_PLUGINS_DIR/$1
  else

    if [ -d $RELATIVE_CUSTOM_PLUGINS_DIR/$1 ]
    then
      target=$RELATIVE_CUSTOM_PLUGINS_DIR/$1
    else
      echo "Oups ! Plugin $1 does not exist."
      return
    fi
  fi
  cd $ENABLED_PLUGINS_DIR
  ln -s $target $1
  cd $DIRECTORY
  node $DIRECTORY/edition_manager.js -plugin $1
  echo -e "Plugin $1 successfully enabled."
  echo -e ''
}

disable_plugin(){
  echo -e ''
  if [ ! -d $ENABLED_PLUGINS_DIR/$1 ]
  then
      echo -e "Plugin $1 was not enabled."
      echo -e ''
      return
  fi

  rm $ENABLED_PLUGINS_DIR/$1

  echo -e "Plugin $1 successfully disabled."
  echo -e ''
}

enable_project_plugins(){
  config_file=$DIRECTORY/../editions/$1/$1_config_inc.xml
  plugins=$($BASEX_BIN -i$config_file "for \$i in //plugins/plugin/@name return string(\$i)")
  for p in $plugins
    do enable_plugin $p
  done
}

# 1 argument required
if [  $# -lt 1 ]
then
    display_usage
    exit 1
fi


NEW_EDITION_MODE='-n'
DEMO_MODE='-d'
INIT_MODE='-i'

CHOOSEN_MODE=''

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -h) display_usage
            exit 0;;
        -p) PORT="$2"; shift ;;
        -n|-d|-i) CHOOSEN_MODE="$1"; shift ;;
        --list-plugins) list_plugins;exit 0;;
        --enable-plugin) enable_plugin $2;exit 0;;
        --disable-plugin) disable_plugin $2;exit 0;;
         *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done


if [ -z $CHOOSEN_MODE ]
then
  display_usage
  exit 1
fi

db_demo_feed(){
    db_project_feed max_demo_lorem $DIRECTORY/demo/demo_data/
}
#adds xml datas to a new db
db_project_feed(){
   feed_cmd=$BASEX_CLIENT_BIN" -p$PORT -w -cfeed.txt"
   echo "Please type your BaseX login/password :"
   echo "CREATE DATABASE "$1 > feed.txt
   echo "ADD "$2 >> feed.txt
   eval $feed_cmd
   ret_code=$?
   if [ $ret_code -gt 0 ]
    then echo "Cannot insert data in DB. Is yout BaseX running on "$PORT" ?"
   else
    rm feed.txt
    echo "INFO: The "$1" DB was successfully created."
   fi
}

demo_edition_build(){
  if [ ! -d $DIRECTORY/../editions ]
    then
      echo 'Creates "editions" directory.'
      mkdir $DIRECTORY/../editions
    fi

    if [ -d $DIRECTORY/../editions/demo_lorem ]; then
      echo "Removes existing demo edition."
      rm -rf $DIRECTORY/../editions/demo_lorem
    fi

    cp -r $DIRECTORY/demo/demo_edition $DIRECTORY/../editions/demo_lorem
    cp $DIRECTORY/demo/demo_lorem_config_inc.xml $DIRECTORY/../editions/demo_lorem
    echo "  -> resource files copy: DONE"
    enable_project_plugins demo_lorem
    node $DIRECTORY/edition_manager.js -demo
    if [ $? -ne 0 ]
    then
      echo 'Process failed'
      exit 1
    else
      format_configuration_file
    fi
}

new_edition_build(){
  if [ ! -d $DIRECTORY/../editions ]
  then
    echo 'Creates "editions" directory.'
    mkdir $DIRECTORY/../editions
  fi
  node $DIRECTORY/edition_manager.js -new $1 $2 $3
  format_configuration_file
}

format_configuration_file(){
    FILE=$DIRECTORY"/../configuration/configuration.xml"
    mv $FILE $FILE".bak" || exit 1
    xmllint --format $FILE".bak" > $FILE
    rm $FILE".bak"
}

init_max(){
    cd $DIRECTORY/..
    echo " -> Fetches node dependencies."
    npm install
    echo " -> Adds nodes_modules/.ignore file."
    #creates a .ignore file in nodejs dependencies folder
    touch $DIRECTORY"/../node_modules/.ignore"
    echo "
      *** MaX initialization is finished :) ***
";
}





#creates config file if not exists (.dist copy)
if [ ! -f $DIRECTORY/../configuration/configuration.xml ]
then
    echo "Configuration file does not exists: copying the .dist one"
    cp  $DIRECTORY/../configuration/configuration.dist.xml $DIRECTORY/../configuration/configuration.xml
fi


if [ $CHOOSEN_MODE == $NEW_EDITION_MODE ]
then
    read -e -p "Project ID ? " project_id
    read -e -p "XML Project type (tei, ead, ...) ? " xmlns
    read -e -p "Database path ? " db_path
    new_edition_build $project_id $db_path $xmlns
    read -e -p "XML sources path ? " data_path
    db_project_feed $db_path $data_path
    #mkdir $PWD/../editions/$project_id
    mkdir $DIRECTORY/../editions/$project_id/ui
    mkdir $DIRECTORY/../editions/$project_id/ui/css
    mkdir $DIRECTORY/../editions/$project_id/ui/js
    mkdir $DIRECTORY/../editions/$project_id/ui/images
    touch $DIRECTORY/../editions/$project_id/ui/css/$project_id.css
    cp $DIRECTORY/menu_default.xml $DIRECTORY/../editions/$project_id/menu.xml
    exit 0
fi

if [ $CHOOSEN_MODE == $DEMO_MODE ]
then
    db_demo_feed
    demo_edition_build
    exit 0
fi

if [ $CHOOSEN_MODE == $INIT_MODE ]
then
    init_max
    exit 0
fi
