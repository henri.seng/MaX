(: For conditions of distribution and use, see the accompanying legal.txt file. :)

module namespace max = 'pddn/max';
import module namespace max.config = 'pddn/max/config' at 'rxq/config.xqm';
import module namespace max.util = 'pddn/max/util' at 'rxq/util.xqm';
import module namespace max.html = 'pddn/max/html' at 'rxq/html.xqm';
import module namespace max.toc = 'pddn/max/toc' at 'rxq/toc.xqm';
import module namespace max.api = 'pddn/max/max_api' at 'rxq/max_api.xqm';
import module namespace request = "http://exquery.org/ns/request";


(:MaX Home:)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:encoding("UTF-8")
%rest:path( "/max")
function max:home(){
 <html>
    <head>
        <title>MaX - Moteur d'affichage XML</title>
    </head>
    <body class='maxhome'>
        <h1>MaX - Moteur d'affichage XML</h1>
        <div>Processeur XSLT : {xslt:processor()}</div>
    </body>
 </html>
};

(:MaX Home:)
declare
%rest:GET
%rest:path( "/favicon.ico")
function max:favicon(){
    max.util:rawFile(file:parent(static-base-uri()) || 'ui/images/favicon.ico')
};


(:
declare option db:chop 'false';
:)

(:Project's home:)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:encoding("UTF-8")
%rest:path( "/{$project}")
function max:projectHome($project){
    let $redirection :=
    if(ends-with(request:uri(),'/'))
    then
        request:uri() || 'accueil'
    else
        request:uri() || '/accueil'
    return
        web:redirect($redirection)
};

(:gets a project's HTML page:)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:indent("no")
%output:encoding("UTF-8")
%rest:path( "/{$project}/{$page=[a-zA-Z0-9_]+}")
function max:page($project, $page){
    try {
        switch ($page)
            case "sommaire"
                return
                    let $content := max.toc:getProjectTOC($project)
                    return max.html:render($project, $page, $content, ())
            case "accueil"
            (:if 'accueil' frag does not exists -> display route list by default:)
                return
                    try {
                        let $content := max.html:getHTMLFragment($project, $page)
                        return max.html:render($project, $page, $content, ())
                    }
                    catch * {
                        admin:write-log('Page/fragment "' || $page || '" introuvable ou mal formé', 'INFO'),
                        let $content := max.toc:getProjectTOC($project)
                        return max.html:render($project, $page, $content, ())
                    }

            default return
                try {
                    let $content := max.html:getHTMLFragment($project, $page)
                    return max.html:render($project, $page, $content, ())
                }
                catch err:FODC0002 {
                    max:max-error('Page/fragment ' || $page ||' introuvable ou mal formé', $err:description,$err:module, $err:line-number)
                }
    }
    catch err:config {max:max-error('', $err:description,$err:module, $err:line-number)}
    catch err:FODC0002 {max:max-error('', $err:description,$err:module, $err:line-number)}
    catch err:XPTY0004 {max:max-error('Configuration dupliquée - Veuillez vérifier votre fichier de configuration', $err:description,$err:module, $err:line-number)}
};


declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:indent("no")
%output:encoding("UTF-8")
%rest:path("/{$project}/sommaire/{$doc=.*\.xml|.*\.svg}")
function max:documentTOC($project, $doc){
    let $content := max.toc:getDocumentTOC($project, $doc)
    return max.html:render($project, "sommaire", $content, ())
};

(:gets a project's XML fragment + a navigation bar(wrapped in an html skeleton):)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:indent("no")
%output:encoding("UTF-8")
%rest:path("/{$project}/{$routeDoc=.*\.xml}/{$id}")
function max:fragmentToHTMLPage($project, $routeDoc, $id){
    let $dbPath := max.config:getProjectDBPath($project)
    return
        let $xml :=
            <div id="wrap-{$id}">
                {max.api:getXMLByID($dbPath, $id)}
            </div>
        let $xsltDoc := max.util:buildXSLTDoc(
                max.config:getDefaultTextXSL($project),
                max.config:getXSLTAddons($project, $routeDoc))
        let $xsltParams := max.config:getXSLTParams($project, $routeDoc)
        return
            try {
                let $html := xslt:transform($xml, $xsltDoc, $xsltParams)

                return max.html:render($project, $routeDoc, <div>{max:invokePluginXQueries($project, $routeDoc, $id)}{$html}</div>, $id)
            }
            catch * {
                max:max-error("Erreur code " || $err:code, $err:description,$err:module, $err:line-number)
            }
};

(:
Highlights $search in a subfragment ($targetId) in a parent fragment ($id)
:)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:indent("no")
%output:encoding("UTF-8")
%rest:path("/{$project}/{$routeDoc=.*\.xml}/{$id}/target/{$targetId}/{$search}")
function max:HighlightedSearchToHTMLPage($project, $routeDoc, $id, $targetId, $search){

    copy $c := max:fragmentToHTMLPage($project, $routeDoc, $id)
    modify (
        replace value of node $c/descendant-or-self::*[@*:id = $targetId]/@*:id with "target"
    )
    return $c


};


(:Call the check plugin method (on a specified project:)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:indent("no")
%output:encoding("UTF-8")
%rest:path("/{$project}/checkplugins")
function max:checkPlugin($project){
    let $plugins := max.config:getPluginNameList($project)
    return
        if (not($plugins))
        then <div id='pluginsReport'>Aucun plugin pour {$project}.</div>
        else
            let $reports :=
                <ul>{for $plugin in $plugins
                return
                    try
                    {
                        xquery:invoke(request:uri() || "/../" || $plugin || "/report")
                    }
                    catch err:FODC0002 {<div>Méthode {$plugin || "/report"} manquante.</div>}
                }
                </ul>
            let $report :=
                <div id='pluginsReport'>
                    <div>Installed plugin(s): {string-join($plugins,', ')}</div>
                    {$reports}
                </div>
            return $report
};


(:Returns XML fragment identified by $id:)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:indent("no")
%output:encoding("UTF-8")
%rest:path("/{$project}/fragment/{$id}")
function max:getXMLByID($project, $id){
    max.api:getXMLByID(max.config:getProjectDBPath($project), $id)
};

(:Returns XML fragment identified by $id in its HTML version:)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:indent("no")
%output:encoding("UTF-8")
%rest:query-param("xsl", "{$xsl}")
%rest:query-param("xslparams", "{$xslparams}")
%rest:query-param("wrap", "{$wrap}")
%rest:path("/{$project}/fragment_html/{$id}")
function max:getHTMLByID($project, $id, $xsl, $xslparams as xs:string *, $wrap as xs:string ?) as element(){
    let $xml := max.api:getXMLByID(max.config:getProjectDBPath($project), $id)
    let $xslAddons := (max.config:getXSLTAddons($project,()),if ($xsl) then file:parent(static-base-uri()) || string("editions/" || $project || "/ui/xsl/" || $xsl) else ())
    let $xsltDoc := max.util:buildXSLTDoc(
            max.config:getDefaultTextXSL($project),
            $xslAddons
    )

    let $xslrURLParams := for $param in $xslparams
    let $paramName := substring-before($param, ':')
    let $paramValue := substring-after($param, ':')
    return map:entry($paramName, $paramValue)
    (:Merge all xsl parameters: from url + project:)
    let $xsltParams := map:merge((max.config:getXSLTParams($project, ()), $xslrURLParams))

    let $html := <div class='standalone-html' id="wrap-{$id}">
        {xslt:transform($xml, $xsltDoc, $xsltParams)}
    </div>
    return
        if ($wrap and $wrap = 'true')
        then max.html:wrapInSimpleHTML($project, $html)
        else $html
};

(:returns full document. Transformed if specified in config file, or if default env. xsl exists:)
declare
%rest:GET
%output:method("html")
%output:html-version("5.0")
%output:indent("no")
%output:encoding("UTF-8")
%rest:path("/{$project}/doc/{$doc=.*\.xml|.*\.svg}")
function max:getFullDocument($project, $doc){
    let $xmlDoc := (doc(max.config:getProjectDBPath($project) || "/" || $doc)/*)[1]
    return
        max.html:render(
                $project, "doc/"||$doc,
                <div><div>{max:invokePluginXQueries($project, $doc, ())}</div><div id='text'>{max:transformEditionFragment($project, $xmlDoc, $doc)}</div></div>)
};


declare function max:transformEditionFragment($project, $xml, $route){
    let $xsltDoc := max.util:buildXSLTDoc(
            max.config:getDefaultTextXSL($project),
            max.config:getXSLTAddons($project, $route))
    let $xsltParams := max.config:getXSLTParams($project, $route)
    return xslt:transform($xml, $xsltDoc, $xsltParams)
};

declare
%rest:error("err:max")
%output:method("html")
%output:html-version("5.0")
%output:indent("no")
%output:encoding("UTF-8")
%rest:error-param("message", "{$message}")
%rest:error-param("desc", "{$desc}")
%rest:error-param("module", "{$module}")
%rest:error-param("line", "{$line}")
function max:max-error($message, $desc, $module, $line)
{
    admin:write-log('MaX error - ' || $message),
    <html>
        <head>
            <title>MaX - ERROR</title>
        </head>
        <body>
            <div class='error'>
                <div>{if($message) then $message else 'MaX - Erreur'}</div>
                <div><h2>Trace BaseX</h2>
                    <ul>
                        <li>Description : <span>{$desc}</span></li>
                        <li>Module : <span>{$module}</span></li>
                        <li>Line : <span>{$line}</span></li>
                    </ul>
                </div>
            </div>
        </body>
    </html>
};

declare
%rest:GET
%output:method("xml")
%output:encoding("UTF-8")
%output:omit-xml-declaration("no")
%rest:path("/{$project}/sitemap.xml")
function max:buildSitemap($project){
    max.html:getSitemap($project)
};



declare %private function max:invokePluginXQueries($project, $routeDoc, $id){
    let $dbPath := max.config:getProjectDBPath($project)
    let $pluginsHTML :=
        for $pluginName in max.config:getPluginNameList($project)
        let $pluginXQ := file:parent(static-base-uri())||'/plugins-enabled/' || $pluginName || '/'|| $pluginName ||'.xq'
            return if(file:exists($pluginXQ))
            then
                xquery:invoke($pluginXQ,
                        map
                        {
                        'baseURI' : max.config:getBaseURI(),
                        'dbPath': $dbPath,
                        'project' : $project,
                        'doc' : $routeDoc,
                        'id' :$id
                        })
            else ()
    return $pluginsHTML
};