FROM basex/basexhttp:latest
MAINTAINER Certic-Pdn TEAM <contact.certic@unicaen.fr>
COPY max.xq /srv/basex/webapp
COPY rxq /srv/basex/webapp/rxq
COPY configuration /srv/basex/webapp/configuration
COPY editions /srv/basex/webapp/editions
COPY plugins /srv/basex/webapp/plugins
COPY ui /srv/basex/webapp/ui
COPY node_modules/bootstrap/dist/js/bootstrap.bundle.min.js /srv/basex/webapp/ui/lib
COPY node_modules/bootstrap/dist/css/bootstrap.min.css /srv/basex/webapp/ui/lib
COPY node_modules/bootstrap/dist/js/bootstrap.min.js /srv/basex/webapp/ui/lib
COPY node_modules/jquery/dist/jquery.min.js /srv/basex/webapp/ui/lib
COPY node_modules/popper.js/dist/popper.min.js /srv/basex/webapp/ui/lib
